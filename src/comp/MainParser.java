package comp;

import elem.Airport;
import elem.Utility;
import java.io.File;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

public class MainParser {

    //DEBUG MODE ===========================
    public static final boolean devMode = false;

    public static void main(String[] args) {
        try {
            
            if(args.length != 2){
                System.err.println("usage: g43 <input_file> <output_file>");
                return;                
            }

            CharStream input = new ANTLRFileStream(args[0]);
            XML2SDLLexer lex = new XML2SDLLexer(input);
            CommonTokenStream tokens = new CommonTokenStream(lex);
           

            XML2SDLParser parser = new XML2SDLParser(tokens);

            XML2SDLParser.DocumentContext documentContext = parser.document();

            ParseTreeWalker walker = new ParseTreeWalker();
            XML2SDLListener listener = new XML2SDLListener();
            walker.walk(listener, documentContext);
            
           
            if(parser.getNumberOfSyntaxErrors() != 0){
                return;
            }         

            //Print processed airport
            if (devMode) {
                
//                for (Airport airp : listener.getAirportList()) {
//                    airp.print();
//                    System.out.println("==================================");
//                    System.out.println("==================================" + System.lineSeparator());
//                }

            }
            
            if(listener.getErrorCount() > 0 || listener.getAirportList().isEmpty()){
                System.out.println("Errors found in input file.");
                return;
            }
            
            //Write to file 
            try {
                
                File outputFile = new File(args[1]);
                
                outputFile.createNewFile();
                
                Utility.generateSDLfile(outputFile, listener.getAirportList());              
                
            } catch (Exception e) {
                System.err.println("Main Parser - " + e.getMessage());
            }

        } catch (Throwable t) {
            System.out.println("exception: " + t);
            t.printStackTrace();
        }
    }
}
