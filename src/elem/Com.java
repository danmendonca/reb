package elem;

import java.util.HashSet;

public class Com {

    double frequency;
    String name, type;

    private final static HashSet<String> typeSet;

    static {
        typeSet = new HashSet<>();
        typeSet.add("APPROACH");
        typeSet.add("ASOS");
        typeSet.add("ATIS");
        typeSet.add("AWOS");
        typeSet.add("CENTER");
        typeSet.add("CLEARANCE");
        typeSet.add("CLEARANCE_PRE_TAXI");
        typeSet.add("CTAF");
        typeSet.add("DEPARTURE");
        typeSet.add("FSS");
        typeSet.add("GROUND");
        typeSet.add("MULTICOM");
        typeSet.add("REMOTE_CLEARANCE_DELIVERY");
        typeSet.add("TOWER");
        typeSet.add("UNICOM");
    }

    public Com() {
        name = type = null;
        frequency = Double.NaN;
    }

    /**
     *
     * @throws IllegalArgumentException
     */
    public void checkMissingAttr() throws IllegalArgumentException {
        Utility.checkMandatoryDouble("Frequency", frequency);
        Utility.checkMandatoryString("Type", type);
        Utility.checkMandatoryString("Name", name);
    }

    /**
     *
     * @param type
     */
    public void setType(String type) throws IllegalArgumentException{
        if (Utility.checkDuplicString(this.type, "Type")) {
            String typeStr = type.replaceAll("^\"|\"$", "");

            if (typeSet.contains(typeStr)) {
                this.type = typeStr;
            } else {
                throw new IllegalArgumentException("type: Invalid type");
            }
        }
    }

    /**
     *
     * @param name
     */
    public void setName(String name) throws IllegalArgumentException{
        if (Utility.checkDuplicString(this.name, "Name")) {
            this.name = Utility.setString(name, "Name", 48);
        }
    }

    /**
     *
     * @param frequency
     */
    public void setFrequency(String frequency) throws IllegalArgumentException{
        if (Utility.checkDuplicDouble(this.frequency, "Frequency")) {
            this.frequency = Utility.setDouble(frequency, "frequency", 108, 136.992);
        }
    }

    /**
     *
     */
    public void print() {

        System.out.println("### Com - Attributes ###");

        Utility.printDouble("frequency", this.frequency);
        Utility.printString("type", this.type);
        Utility.printString("name", this.name);

        System.out.print(System.lineSeparator());

    }

}
