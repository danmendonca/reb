/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package elem;

/**
 *
 * @author Daniel
 */
public class TaxiName {

    public String getName() {
        return name;
    }

    int index;
    String name;

    public TaxiName() {
        index = Integer.MAX_VALUE;
        name = null;
    }

    /**
     *
     * @throws IllegalArgumentException
     */
    public void checkMissingAttr() throws IllegalArgumentException {
        Utility.checkMandatoryInt("Index", index);
    }

    /**
     *
     * @param index
     */
    public void setIndex(String index) {
        if (Utility.checkDuplicInt(this.index, "Index")) {
            this.index = Utility.setInt(index, "index", 0, 255);
        }
    }

    /**
     * 
     * @return 
     */
    public int getIndex() {
        return index;
    }
    
    

    /**
     *
     * @param name
     */
    public void setName(String name) {
        if (Utility.checkDuplicString(this.name, "Name")) {
            this.name = Utility.setString(name, "name", 8);
        }
    }

    /**
     * 
     */
    public void print() {

        System.out.println("### TaxiName - Attributes ###");

        Utility.printInt("index", index);
        Utility.printString("name", name);

        System.out.print(System.lineSeparator());
    }
}
