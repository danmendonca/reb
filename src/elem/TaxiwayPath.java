package elem;

import java.util.HashSet;

public class TaxiwayPath {

    private int start = Integer.MAX_VALUE;
    private boolean startSet;
    private int end = Integer.MAX_VALUE;
    private boolean endSet;
    private int name = Integer.MAX_VALUE;
    private boolean nameSet;

    private double weightLimit;

    private String type;
    private String surface;
    private String drawSurface;
    private String drawDetail;
    private String centerLine;
    private String centerLineLighted;
    private String leftEdge;
    private String leftEdgeLighted;
    private String rightEdge;
    private String rightEdgeLighted;
    private String number;
    private String designator;
    private String width;

    private final static HashSet<String> typeSet;
    private final static HashSet<String> surfaceSet;
    private final static HashSet<String> designatorSet;

    static {
        typeSet = new HashSet<>();
        typeSet.add("RUNWAY");
        typeSet.add("PARKING");
        typeSet.add("TAXI");
        typeSet.add("PATH");
        typeSet.add("CLOSED");
        typeSet.add("VEHICLE");

        surfaceSet = new HashSet<>();
        surfaceSet.add("ASPHALT");
        surfaceSet.add("BITUMINOUS");
        surfaceSet.add("BRICK");
        surfaceSet.add("CLAY");
        surfaceSet.add("CEMENT");
        surfaceSet.add("CONCRETE");
        surfaceSet.add("CORAL");
        surfaceSet.add("DIRT");
        surfaceSet.add("GRASS");
        surfaceSet.add("GRAVEL");
        surfaceSet.add("ICE");
        surfaceSet.add("MACADAM");
        surfaceSet.add("OIL_TREATED");
        surfaceSet.add("PLANKS");
        surfaceSet.add("SAND");
        surfaceSet.add("SHALE");
        surfaceSet.add("SNOW");
        surfaceSet.add("STEEL_MATS");
        surfaceSet.add("TARMAC");
        surfaceSet.add("UNKNOWN");
        surfaceSet.add("WATER");

        designatorSet = new HashSet<>();
        designatorSet.add("NONE");
        designatorSet.add("C");
        designatorSet.add("CENTER");
        designatorSet.add("L");
        designatorSet.add("LEFT");
        designatorSet.add("R");
        designatorSet.add("RIGHT");
        designatorSet.add("W");
        designatorSet.add("WATER");
        designatorSet.add("A");
        designatorSet.add("B");
    }

    public TaxiwayPath() {
        weightLimit = Double.NaN;
        startSet = false;
        endSet = false;
        nameSet = false;
    }

    /**
     *
     */
    public void print() {

        System.out.println("### TaxiwayPath - Attributes ###");

        if (startSet) {
            System.out.println("Start = " + start);
        }

        if (endSet) {
            System.out.println("End = " + end);
        }

        if (!Double.isNaN(weightLimit)) {
            System.out.println("WeightLimit = " + weightLimit);
        }

        if ((type != null) && !type.isEmpty()) {
            System.out.println("Type = " + type);
        }

        if ((surface != null) && !surface.isEmpty()) {
            System.out.println("Surface = " + surface);
        }

        if ((drawSurface != null) && !drawSurface.isEmpty()) {
            System.out.println("DrawSurface = " + drawSurface);
        }

        if ((drawDetail != null) && !drawDetail.isEmpty()) {
            System.out.println("DrawDetail = " + drawDetail);
        }

        if ((centerLine != null) && !centerLine.isEmpty()) {
            System.out.println("CenterLine = " + centerLine);
        }

        if ((centerLineLighted != null) && !centerLineLighted.isEmpty()) {
            System.out.println("CenterLineLighted = " + centerLineLighted);
        }

        if ((leftEdge != null) && !leftEdge.isEmpty()) {
            System.out.println("LeftEdge = " + leftEdge);
        }

        if ((leftEdgeLighted != null) && !leftEdgeLighted.isEmpty()) {
            System.out.println("LeftEdgeLighted = " + leftEdgeLighted);
        }

        if ((rightEdge != null) && !rightEdge.isEmpty()) {
            System.out.println("RightEdge = " + rightEdge);
        }

        if ((rightEdgeLighted != null) && !rightEdgeLighted.isEmpty()) {
            System.out.println("RightEdgeLighted = " + rightEdgeLighted);
        }

        if ((number != null) && !number.isEmpty()) {
            System.out.println("Number = " + number);
        }

        if ((designator != null) && !designator.isEmpty()) {
            System.out.println("Designator = " + designator);
        }

        if ((width != null) && !width.isEmpty()) {
            System.out.println("Width = " + width);
        }

        if (nameSet) {
            System.out.println("Name = " + name);
        }

        System.out.print(System.lineSeparator());

    }

    /**
     *
     * @throws IllegalArgumentException
     */
    public void checkMissingAttr() throws IllegalArgumentException {
        Utility.checkMandatoryString("Type", type);
        Utility.checkMandatoryInt("Start", start);
        Utility.checkMandatoryInt("End", end);
        Utility.checkMandatoryString("Width", width);
        Utility.checkMandatoryDouble("WeightLimit", weightLimit);
        Utility.checkMandatoryString("Surface", surface);
        Utility.checkMandatoryString("Draw Surface", drawSurface);
        Utility.checkMandatoryString("Draw Detail", drawDetail);
        //Utility.checkMandatoryString("Number", number);
        //Utility.checkMandatoryString("Designator", designator);
        //Utility.checkMandatoryInt("Name", name);
    }

    /**
     *
     * @param input
     */
    public void setType(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(type, "Type")) {
            String typeStr = input.replaceAll("^\"|\"$", "");

            if (typeSet.contains(typeStr)) {
                type = typeStr;
            } else {
                throw new IllegalArgumentException("Type: Invalid type");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setStart(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicInt(start, "Start")) {
            String startStr = input.replaceAll("^\"|\"$", "");
            start = Integer.parseInt(startStr);
            startSet = true;
        }
    }

    /**
     *
     * @param input
     */
    public void setEnd(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicInt(end, "End")) {
            String endStr = input.replaceAll("^\"|\"$", "");
            end = Integer.parseInt(endStr);
            endSet = true;
        }
    }

    /**
     *
     * @param input
     */
    public void setWidth(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicString(width, "Width")) {
            width = Utility.setMeterFeet(input, "Width");
        }
    }

    /**
     *
     * @param input
     */
    public void setWeightLimit(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicDouble(weightLimit, "WeightLimit")) {
            weightLimit = Utility.setDouble(input, "WeightLimit", 0.0, Double.MAX_VALUE);
        }
    }

    /**
     *
     * @param input
     */
    public void setSurface(String input) throws IllegalArgumentException {

        if (Utility.checkDuplicString(surface, "Surface")) {
            String surfaceStr = input.replaceAll("^\"|\"$", "");

            if (surfaceSet.contains(surfaceStr)) {
                surface = surfaceStr;
            } else {
                throw new IllegalArgumentException("Surface: Invalid surface");
            }
        }

    }

    /**
     *
     * @param input
     */
    public void setDrawSurface(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(drawSurface, "DrawSurface")) {
            String drawSurfaceStr = input.replaceAll("^\"|\"$", "");

            switch (drawSurfaceStr) {
                case "TRUE":
                    drawSurface = "TRUE";
                    break;
                case "FALSE":
                    drawSurface = "FALSE";
                    break;
                default:
                    throw new IllegalArgumentException("Draw Surface: Invalid value");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setDrawDetail(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(drawDetail, "Draw Detail")) {
            String drawDetailStr = input.replaceAll("^\"|\"$", "");

            switch (drawDetailStr) {
                case "TRUE":
                    drawDetail = "TRUE";
                    break;
                case "FALSE":
                    drawDetail = "FALSE";
                    break;
                default:
                    throw new IllegalArgumentException("Draw Detail: Invalid value");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setCenterLine(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(centerLine, "Center Line")) {
            String str = input.replaceAll("^\"|\"$", "");

            switch (str) {
                case "TRUE":
                    centerLine = "TRUE";
                    break;
                case "FALSE":
                    centerLine = "FALSE";
                    break;
                default:
                    throw new IllegalArgumentException("Center Line: Invalid value");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setCenterLineLighted(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(centerLineLighted, "Center Line Lighted")) {
            String str = input.replaceAll("^\"|\"$", "");

            switch (str) {
                case "TRUE":
                    centerLineLighted = "TRUE";
                    break;
                case "FALSE":
                    centerLineLighted = "FALSE";
                    break;
                default:
                    throw new IllegalArgumentException("Center Line Lighted: Invalid value");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setLeftEdge(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(leftEdge, "LeftEdge")) {
            String str = input.replaceAll("^\"|\"$", "");

            switch (str) {
                case "NONE":
                    leftEdge = "NONE";
                    break;
                case "SOLID":
                    leftEdge = "SOLID";
                    break;
                case "DASHED":
                    leftEdge = "DASHED";
                    break;
                case "SOLID_DASHED":
                    leftEdge = "SOLID_DASHED";
                    break;
                default:
                    throw new IllegalArgumentException("Left Edge: Invalid value");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setLeftEdgeLighted(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(leftEdgeLighted, "Left Edge Lighted")) {
            String str = input.replaceAll("^\"|\"$", "");

            switch (str) {
                case "TRUE":
                    leftEdgeLighted = "TRUE";
                    break;
                case "FALSE":
                    leftEdgeLighted = "FALSE";
                    break;
                default:
                    throw new IllegalArgumentException("Left Edge Lighted: Invalid value");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setRightEdge(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(rightEdge, "Right Edge")) {
            String str = input.replaceAll("^\"|\"$", "");

            switch (str) {
                case "NONE":
                    rightEdge = "NONE";
                    break;
                case "SOLID":
                    rightEdge = "SOLID";
                    break;
                case "DASHED":
                    rightEdge = "DASHED";
                    break;
                case "SOLID_DASHED":
                    rightEdge = "SOLID_DASHED";
                    break;
                default:
                    throw new IllegalArgumentException("Right Edge: Invalid value");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setRightEdgeLighted(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(rightEdgeLighted, "Right Edge Lighted")) {
            String str = input.replaceAll("^\"|\"$", "");

            switch (str) {
                case "TRUE":
                    leftEdgeLighted = "TRUE";
                    break;
                case "FALSE":
                    leftEdgeLighted = "FALSE";
                    break;
                default:
                    throw new IllegalArgumentException("Right Edge Lighted: Invalid value");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setNumber(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicString(number, "Number")) {
            number = input.replaceAll("^\"|\"$", "");
        }
    }

    /**
     *
     * @param input
     */
    public void setDesignator(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(designator, "Designator")) {
            String str = input.replaceAll("^\"|\"$", "");

            if (designatorSet.contains(str)) {
                designator = str;
            } else {
                throw new IllegalArgumentException("Designator: Invalid value");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setName(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicInt(name, "Name")) {
            String str = input.replaceAll("^\"|\"$", "");
            name = Integer.parseInt(str);
            nameSet = true;

            if ((name < 0) || (name > 255)) {
                nameSet = false;
                throw new IllegalArgumentException("Name: Valid integer range is 0 to 255");
            }
        }
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }

    public String getDesignator() {
        return designator;
    }

    public String getType() {
        return type;
    }

    public boolean isStartSet() {
        return startSet;
    }

    public boolean isEndSet() {
        return endSet;
    }

    public int getName() {
        return name;
    }

    public boolean isNameSet() {
        return nameSet;
    }

    public double getWeightLimit() {
        return weightLimit;
    }

    public String getSurface() {
        return surface;
    }

    public String getDrawSurface() {
        return drawSurface;
    }

    public String getDrawDetail() {
        return drawDetail;
    }

    public String getCenterLine() {
        return centerLine;
    }

    public String getCenterLineLighted() {
        return centerLineLighted;
    }

    public String getLeftEdge() {
        return leftEdge;
    }

    public String getLeftEdgeLighted() {
        return leftEdgeLighted;
    }

    public String getRightEdge() {
        return rightEdge;
    }

    public String getRightEdgeLighted() {
        return rightEdgeLighted;
    }

    public String getNumber() {
        return number;
    }

    public String getWidth() {
        return width;
    }

    public static HashSet<String> getTypeSet() {
        return typeSet;
    }

    public static HashSet<String> getSurfaceSet() {
        return surfaceSet;
    }

    public static HashSet<String> getDesignatorSet() {
        return designatorSet;
    }
    
    
    
    
    

}
