/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package elem;

import java.util.HashSet;

/**
 *
 * @author Daniel
 */
public class Helipad {

    double heading;
    String lat, lon;
    String alt, length, width, type, surface;
    String closed, transparent;
    int red, green, blue;
    private final static HashSet<String> surfaceSet;
    private final static HashSet<String> typeSet;

    static {
        surfaceSet = new HashSet<>();
        surfaceSet.add("ASPHALT");
        surfaceSet.add("BITUMINOUS");
        surfaceSet.add("BRICK");
        surfaceSet.add("CLAY");
        surfaceSet.add("CEMENT");
        surfaceSet.add("CONCRETE");
        surfaceSet.add("CORAL");
        surfaceSet.add("DIRT");
        surfaceSet.add("GRASS");
        surfaceSet.add("GRAVEL");
        surfaceSet.add("ICE");
        surfaceSet.add("MACADAM");
        surfaceSet.add("OIL_TREATED");
        surfaceSet.add("PLANKS");
        surfaceSet.add("SAND");
        surfaceSet.add("SHALE");
        surfaceSet.add("SNOW");
        surfaceSet.add("STEEL_MATS");
        surfaceSet.add("TARMAC");
        surfaceSet.add("UNKNOWN");
        surfaceSet.add("WATER");

        typeSet = new HashSet<>();
        typeSet.add("NONE");
        typeSet.add("CIRCLE");
        typeSet.add("H");
        typeSet.add("MEDICAL");
        typeSet.add("SQUARE");
    }

    public Helipad() {
        this.lat = this.lon = null;
        this.heading = Double.NaN;
        this.red = this.blue = this.green = Integer.MAX_VALUE;
        this.closed = this.transparent = null;
    }

    /**
     *
     * @throws IllegalArgumentException
     */
    public void checkMissingAttr() throws IllegalArgumentException {
        Utility.checkMandatoryString("Lat", lat);
        Utility.checkMandatoryString("Lon", lon);
        Utility.checkMandatoryString("Alt", alt);
        Utility.checkMandatoryString("Surface", surface);
        Utility.checkMandatoryDouble("Heading", heading);
        Utility.checkMandatoryString("Length", length);
        Utility.checkMandatoryString("Width", width);
        Utility.checkMandatoryString("Type", type);
    }

    /**
     *
     * @param input
     */
    public void setSurface(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(surface, "Surface")) {
            String surfaceStr = input.replaceAll("^\"|\"$", "");

            if (surfaceSet.contains(surfaceStr)) {
                surface = surfaceStr;
            } else {
                throw new IllegalArgumentException("Surface: Invalid surface");
            }
        }
    }

    /**
     *
     * @param input
     * @throws NumberFormatException
     * @throws IllegalArgumentException
     */
    public void setLat(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicString(lat, "Lat")) {
            String dmsFormat = Utility.setDegMinSec(input, 90);

            if (dmsFormat != null) {
                this.lat = dmsFormat;
                return;
            }

            this.lat = "" + Utility.setDouble(input, "lat", -90, 90);
        }
    }

    /**
     *
     * @param input
     */
    public void setLon(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicString(lon, "Lon")) {
            String dmsFormat = Utility.setDegMinSec(input, 90);

            if (dmsFormat != null) {
                this.lon = dmsFormat;
                return;
            }

            this.lon = "" + Utility.setDouble(input, "lon", -180, 180);
        }
    }

    /**
     *
     * @param heading
     */
    public void setHeading(String heading) {
        if (Utility.checkDuplicDouble(this.heading, "Heading")) {
            this.heading = Utility.setDouble(heading, "heading", 0, 360);
        }
    }

    /**
     *
     * @param input
     */
    public void setAlt(String input) {
        if (Utility.checkDuplicString(alt, "Alt")) {
            this.alt = Utility.setMeterFeet(input, "alt");
        }
    }

    /**
     *
     * @param length
     */
    public void setLength(String length) {
        if (Utility.checkDuplicString(this.length, "Length")) {
            this.length = Utility.setMeterFeet(length, "length");
        }
    }

    /**
     *
     * @param width
     */
    public void setWidth(String width) {
        if (Utility.checkDuplicString(this.width, "Width")) {
            this.width = Utility.setMeterFeet(width, "width");
        }
    }

    /**
     *
     * @param type
     */
    public void setType(String type) {
        if (Utility.checkDuplicString(this.type, "Type")) {
            type = type.replaceAll("^\"|\"$", "");
            if (!typeSet.contains(type)) {
                throw new IllegalArgumentException("Surface: Invalid surface");
            }

            this.type = type;
        }
    }

    /**
     *
     * @param closed
     */
    public void setClosed(String closed) {
        if (Utility.checkDuplicString(this.closed, "Closed")) {
            this.closed = Utility.setBoolean(closed, "closed");
        }
    }

    /**
     *
     * @param transparent
     */
    public void setTransparent(String transparent) {
        if (Utility.checkDuplicString(this.transparent, "Transparent")) {
            this.transparent = Utility.setBoolean(transparent, "transparent");
        }
    }

    /**
     *
     * @param red
     */
    public void setRed(String red) {
        if (Utility.checkDuplicInt(this.red, "Red")) {
            this.red = Utility.setInt(red, "red", 0, 255);
        }
    }

    /**
     *
     * @param green
     */
    public void setGreen(String green) {
        if (Utility.checkDuplicInt(this.green, "Green")) {
            this.green = Utility.setInt(green, "blue", 0, 255);
        }
    }

    /**
     *
     * @param blue
     */
    public void setBlue(String blue) {
        if (Utility.checkDuplicInt(this.blue, "Blue")) {
            this.blue = Utility.setInt(blue, "blue", 0, 255);
        }
    }

    /**
     *
     */
    public void print() {
        System.out.println("### Helipad - Attributes ###");

        Utility.printString("lat", lat);
        Utility.printString("lon", lon);
        Utility.printDouble("heading", heading);

        Utility.printString("alt", alt);
        Utility.printString("surface", surface);
        Utility.printString("length", length);
        Utility.printString("width", width);
        Utility.printString("type", type);

        Utility.printString("closed", closed);
        Utility.printString("transparent", transparent);

        Utility.printInt("red", red);
        Utility.printInt("green", green);
        Utility.printInt("blue", blue);

        System.out.print(System.lineSeparator());
    }

}
