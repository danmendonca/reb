package elem;

import java.util.ArrayList;
import java.util.HashMap;
import org.jgrapht.graph.DefaultDirectedGraph;
import Graph.RelationshipEdge;
import comp.MainParser;
import java.util.Set;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.ClassBasedEdgeFactory;
import org.jgrapht.graph.DirectedMultigraph;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class Airport {

    /**
     *
     */
    private DefaultDirectedGraph dg;
    private DirectedGraph<Integer, RelationshipEdge> graph;
    private double lat;
    private double lon;
    private double magvar;
    private double trafficScalar;

    private String region;
    private String country;
    private String state;
    private String city;
    private String name;
    private String alt;
    private String ident;
    private String airportTestRadius;

    /**
     *
     */
    private HashMap<Integer, TaxiwayPoint> twPointMap;
    private Services servicesElem;
    private ArrayList<Tower> towerList;
    private HashMap<Integer, TaxiwayParking> twParkingMap;
    private ArrayList<TaxiwayPath> twPathList;
    private ArrayList<Runway> runwayList;
    private ArrayList<Helipad> helipadList;
    private ArrayList<Com> comList;
    private HashMap<Integer, TaxiName> taxiNameMap;

    public void setDg() {

        graph = new DirectedMultigraph<>(new ClassBasedEdgeFactory<Integer, RelationshipEdge>(RelationshipEdge.class));

        for (TaxiwayPath path : twPathList) {

            graph.addVertex(path.getStart());
            graph.addVertex(path.getEnd());

            if (path.getType().equalsIgnoreCase("RUNWAY")) {
                graph.addEdge(path.getStart(), path.getEnd(), new RelationshipEdge<>(path.getStart(), path.getEnd(), path.getType(), 0, path.getNumber()));
            } else if (path.getType().equalsIgnoreCase("TAXI")) {

                RelationshipEdge taxiEdge = new RelationshipEdge<>(path.getStart(), path.getEnd(), path.getType(), path.getName(), null);
                taxiEdge.setSurface(path.getSurface());
                taxiEdge.setWidth(path.getWidth());
                graph.addEdge(path.getStart(), path.getEnd(), taxiEdge);
            }

        }

        if (MainParser.devMode) {
            for (RelationshipEdge edge : graph.edgeSet()) {

                if (edge.getLabel().equalsIgnoreCase("RUNWAY")) {
                    System.out.println(edge.getV1() + " -> " + edge.getV2() + " - " + edge.getLabel() + " = " + edge.getNumber());
                } else if (edge.getLabel().equalsIgnoreCase("TAXI")) {
                    System.out.println(edge.getV1() + " -> " + edge.getV2() + " - " + edge.getLabel() + " = " + edge.getName());
                } else {
                    System.out.println(edge.getV1() + " -> " + edge.getV2() + " - " + edge.getLabel());
                }

            }

            System.out.println("============================================" + System.lineSeparator());
        }

    }

    /**
     *
     */
    public Airport() {
        lat = Double.NaN;
        lon = Double.NaN;
        magvar = Double.NaN;
        trafficScalar = Double.NaN;

        twPointMap = new HashMap<>();

        dg = new DefaultDirectedGraph(RelationshipEdge.class);
    }

    /**
     *
     * @param twPointMap
     */
    public void setTwPointMap(HashMap<Integer, TaxiwayPoint> twPointMap) {
        this.twPointMap = twPointMap;
    }

    /**
     *
     * @param servicesElem
     */
    public void setServicesElem(Services servicesElem) {
        this.servicesElem = servicesElem;
    }

    /**
     *
     * @param towerList
     */
    public void setTowerList(ArrayList<Tower> towerList) {
        this.towerList = towerList;
    }

    /**
     *
     * @param twParkingMap
     */
    public void setTwParkingMap(HashMap<Integer, TaxiwayParking> twParkingMap) {
        this.twParkingMap = twParkingMap;
    }

    /**
     *
     * @param twPathList
     */
    public void setTwPathList(ArrayList<TaxiwayPath> twPathList) {
        this.twPathList = twPathList;
    }

    /**
     *
     * @param runwayList
     */
    public void setRunwayList(ArrayList<Runway> runwayList) {
        this.runwayList = runwayList;
    }

    /**
     *
     * @param helipadList
     */
    public void setHelipadList(ArrayList<Helipad> helipadList) {
        this.helipadList = helipadList;
    }

    /**
     *
     * @param comList
     */
    public void setComList(ArrayList<Com> comList) {
        this.comList = comList;
    }

    /**
     *
     * @param taxiNameMap
     */
    public void setTaxiNameMap(HashMap<Integer, TaxiName> taxiNameMap) {
        this.taxiNameMap = taxiNameMap;
    }

    /**
     *
     */
    public void print() {
        System.out.println("### Airport - Attributes ###");
        Utility.printString("Region", region);
        Utility.printString("Country", country);
        Utility.printString("State", state);
        Utility.printString("City", city);
        Utility.printString("Name", name);
        Utility.printDouble("Lat", lat);
        Utility.printDouble("Lon", lon);
        Utility.printString("Alt", alt);
        Utility.printDouble("Magvr", magvar);
        Utility.printString("Ident", ident);
        Utility.printString("AirportTestRadios", airportTestRadius);
        Utility.printDouble("TrafficScalar", trafficScalar);
        System.out.print(System.lineSeparator());

        servicesElem.print();
        System.out.print(System.lineSeparator());

        for (Tower t : towerList) {
            t.print();
        }
        System.out.print(System.lineSeparator());

        for (Runway rw : runwayList) {
            rw.print();
        }
        System.out.print(System.lineSeparator());

        for (Helipad hp : helipadList) {
            hp.print();
        }
        System.out.print(System.lineSeparator());

        for (Com comElem : comList) {
            comElem.print();
        }
        System.out.print(System.lineSeparator());

        for (TaxiwayPoint twpoint : twPointMap.values()) {
            twpoint.print();
        }
        System.out.print(System.lineSeparator());

        for (TaxiwayParking twparking : twParkingMap.values()) {
            twparking.print();
        }
        System.out.print(System.lineSeparator());

        for (TaxiName txname : taxiNameMap.values()) {
            txname.print();
        }
        System.out.print(System.lineSeparator());

        for (TaxiwayPath twpath : twPathList) {
            twpath.print();
        }
        System.out.print(System.lineSeparator());
    }

    /**
     *
     * @throws IllegalArgumentException
     */
    public void checkMissingAttr() throws IllegalArgumentException {
        Utility.checkMandatoryDouble("Lat", lat);
        Utility.checkMandatoryDouble("Lon", lon);
        Utility.checkMandatoryString("Alt", alt);
        Utility.checkMandatoryString("Ident", ident);
        Utility.checkMandatoryString("AirportTestRadius", airportTestRadius);
        Utility.checkMandatoryDouble("TrafficScalar", trafficScalar);
    }

    /**
     *
     * @param input
     */
    public void setRegion(String input) throws IllegalArgumentException {

        if (Utility.checkDuplicString(region, "Region")) {
            region = Utility.setString(input, "Region", 48);
        }
    }

    /**
     *
     * @param input
     */
    public void setCountry(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(country, "Country")) {
            country = Utility.setString(input, "Country", 48);
        }
    }

    /**
     *
     * @param input
     */
    public void setState(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(state, "State")) {
            state = Utility.setString(input, "State", 48);
        }
    }

    /**
     *
     * @param input
     */
    public void setCity(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(city, "City")) {
            city = Utility.setString(input, "City", 48);
        }
    }

    /**
     *
     * @param input
     */
    public void setName(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(name, "Name")) {
            name = Utility.setString(input, "Name", 48);
        }
    }

    /**
     *
     * @param input
     */
    public void setLat(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicDouble(lat, "Lat")) {
            lat = Utility.setDouble(input, "Lat", -90.0, 90.0);
        }
    }

    /**
     *
     * @param input
     */
    public void setLon(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicDouble(lon, "Lon")) {
            lon = Utility.setDouble(input, "Lon", -180.0, 180.0);
        }
    }

    /**
     *
     * @param input
     */
    public void setAlt(String input) throws IllegalArgumentException, NumberFormatException {
        if (Utility.checkDuplicString(alt, "Alt")) {
            alt = Utility.setMeterFeet(input, "Alt");
        }
    }

    /**
     *
     * @param input
     */
    public void setMagvar(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicDouble(magvar, "Magvar")) {
            magvar = Utility.setDouble(input, "Magvar", -360.0, 360.0);
        }
    }

    /**
     *
     * @param input
     */
    public void setIdent(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(ident, "Ident")) {
            ident = Utility.setString(input, "Ident", 4);
        }
    }

    /**
     *
     * @param input
     */
    public void setAirportTestRadius(String input) throws IllegalArgumentException, NumberFormatException {
        if (Utility.checkDuplicString(airportTestRadius, "AirportTestRadius")) {
            airportTestRadius = Utility.setNauticalMeterFeet(input, "AirportTestRadius");
        }
    }

    /**
     *
     * @param input
     */
    public void setTrafficScalar(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicDouble(trafficScalar, "TrafficScalar")) {
            trafficScalar = Utility.setDouble(input, "TrafficScalar", 0.01, 1.0);
        }
    }

    /**
     *
     * @param index
     * @return
     */
    public TaxiwayPoint getTWPoint(Integer index) {
        return twPointMap.get(index);
    }

    /**
     *
     * @param index
     * @return
     */
    public TaxiwayParking getTWParking(Integer index) {
        return twParkingMap.get(index);
    }

    /**
     *
     * @return
     */
    public ArrayList<TaxiwayPath> getTwPathList() {
        return twPathList;
    }

    /**
     *
     * @param index
     * @return
     */
    public TaxiName getTaxiName(Integer index) {
        return taxiNameMap.get(index);
    }

    /**
     *
     * @param doc
     * @param pos
     * @return
     */
    public Element generateSDLairport(Document doc, int pos) {

        //Base of operations
        Element baseOpElem = generateSDLbaseOperations(doc, pos);

        //Airport
        Element airportElem = doc.createElement("airport");
        airportElem.appendChild(Utility.createElemTextInside(doc, "name", this.name));
        airportElem.appendChild(Utility.createElemTextInside(doc, "description", "XXX Description XXX"));

        //ContactPerson
        airportElem.appendChild(createContactPersonElem(doc));

        //Location
        airportElem.appendChild(createLocationElem(doc));

        //ICAO
        airportElem.appendChild(Utility.createElemTextInside(doc, "ICAO", this.ident));

        //IATA
        airportElem.appendChild(Utility.createElemTextInside(doc, "IATA", "XXX"));

        //magVar
        airportElem.appendChild(Utility.createElemTextInside(doc, "magVar", String.valueOf(this.magvar)));

        //Create Graph
        setDg();

        //Runways
        Element runways = doc.createElement("runways");
        for (Runway rw : runwayList) {

            runways.appendChild(createRunwayElem(doc, rw));

        }
        airportElem.appendChild(runways);

        //Helipad
        Element heliElem = doc.createElement("helipads");

        int heliCount = 1;
        for (Helipad hp : helipadList) {

            Element hpElem = doc.createElement("helipad");
            hpElem.setAttribute("id", "h" + heliCount);
            hpElem.appendChild(Utility.createElemTextInside(doc, "designation", "XXX"));
            hpElem.appendChild(Utility.createElemTextInside(doc, "surface", hp.surface));

            //Coord
            Element coord = createCoordElem(doc, hp.lat, hp.lon, hp.alt);
            hpElem.appendChild(coord);

            hpElem.appendChild(Utility.createElemTextInside(doc, "radius", "XXX"));

            heliElem.appendChild(hpElem);
            heliCount++;
        }
        airportElem.appendChild(heliElem);

        //Taxiways
        //Generate Taxiways SDL
        Element taxiways = taxiways(doc);
        airportElem.appendChild(taxiways);

        //Parking spaces
        //Generate Taxiways SDL
        Element parkingSpaces = parkingSpaces(doc);
        airportElem.appendChild(parkingSpaces);

        //Hangars
        airportElem.appendChild(doc.createElement("hangars"));

        //utilities
        Element utilities = doc.createElement("utilities");

        //tower
        int towerCount = 1;
        for (Tower t : towerList) {

            Element towerElem = doc.createElement("tower");
            towerElem.setAttribute("id", "u" + towerCount);
            towerElem.appendChild(Utility.createElemTextInside(doc, "designation", "Tower"));
            towerElem.appendChild(createCoordElem(doc, String.valueOf(t.lat), String.valueOf(t.lon), t.alt));

            Element radius = Utility.createElemTextInside(doc, "radius", "2.55");
            radius.setAttribute("lengthUnit", "Meter");
            towerElem.appendChild(radius);

            Element height = Utility.createElemTextInside(doc, "height", "129");
            height.setAttribute("lengthUnit", "Meter");
            towerElem.appendChild(height);

            utilities.appendChild(towerElem);
        }
        airportElem.appendChild(utilities);

        baseOpElem.appendChild(airportElem);
        return baseOpElem;
    }

    /**
     *
     * @param doc
     * @param pos
     * @return
     */
    private Element generateSDLbaseOperations(Document doc, int pos) {
        Element baseOpElem = doc.createElement("baseOfOperations");
        baseOpElem.setAttribute("id", "b" + pos);

        //Name
        Element nameElem = Utility.createElemTextInside(doc, "name", this.name);
        baseOpElem.appendChild(nameElem);

        //Mobility
        Element mobilityElem = doc.createElement("mobility");
        String[] arrayMobAttr = {"water", "underwater", "land", "air"};
        String[] arrayMobVal = {"true", "false", "true", "true"};

        for (int i = 0; i < arrayMobAttr.length; i++) {
            mobilityElem.setAttribute(arrayMobAttr[i], arrayMobVal[i]);
        }
        baseOpElem.appendChild(mobilityElem);

        //description
        Element descElem = Utility.createElemTextInside(doc, "description", "XXX Description XXX");
        baseOpElem.appendChild(descElem);

        //history
        Element histElem = Utility.createElemTextInside(doc, "history", "XXX History XXX");
        baseOpElem.appendChild(histElem);

        //contactPerson
        Element cpElem = createContactPersonElem(doc);
        baseOpElem.appendChild(cpElem);

        //location
        Element locElem = createLocationElem(doc);
        baseOpElem.appendChild(locElem);

        //Availability
        Element avaElem = doc.createElement("availability");
        avaElem.setAttribute("available", "always");
        baseOpElem.appendChild(avaElem);

        return baseOpElem;
    }

    /**
     *
     * @param doc
     * @return
     */
    private Element createLocationElem(Document doc) {

        Element locElem = doc.createElement("location");

        locElem.appendChild(Utility.createElemTextInside(doc, "address", "xxxxxxxxxxxxx"));
        locElem.appendChild(Utility.createElemTextInside(doc, "zipCode", "xxxxxxxxxxxxx"));

        locElem.appendChild(Utility.createElemTextInside(doc, "city", this.city));

        if ((this.state != null) && !this.state.isEmpty()) {
            locElem.appendChild(Utility.createElemTextInside(doc, "stateDistrictRegion", this.state));
        } else {
            locElem.appendChild(Utility.createElemTextInside(doc, "stateDistrictRegion", "XXX State XXX"));
        }

        locElem.appendChild(Utility.createElemTextInside(doc, "country", this.country));

        //Coord
        Element coord = createCoordElem(doc, String.valueOf(lat), String.valueOf(lon), alt);
        locElem.appendChild(coord);

        return locElem;
    }

    /**
     *
     * @param doc
     * @return
     */
    private Element createContactPersonElem(Document doc) {

        Element cpElem = doc.createElement("contactPerson");
        String[] arrayCP = {"name",
            "title",
            "institution",
            "position",
            "address",
            "zipCode",
            "city",
            "stateDistrictRegion",
            "country",
            "telephone",
            "cellphone",
            "fax",
            "email"};

        for (String fieldText : arrayCP) {
            cpElem.appendChild(Utility.createElemTextInside(doc, fieldText, "XXX " + fieldText + " XXX"));
        }

        return cpElem;
    }

    /**
     *
     * @param doc
     * @param lat
     * @param lon
     * @param alt
     * @return
     */
    private Element createCoordElem(Document doc, String lat, String lon, String alt) {

        Element coord = doc.createElement("coordinates");
        coord.appendChild(Utility.createElemTextInside(doc, "latitude", lat));
        coord.appendChild(Utility.createElemTextInside(doc, "longitude", lon));
        Element altElem = Utility.createElemTextInside(doc, "altitude", alt);
        altElem.setAttribute("measured", "amsl");
        coord.appendChild(altElem);

        return coord;
    }

    /**
     *
     * @param doc
     * @param rw
     * @return
     */
    private Element createRunwayElem(Document doc, Runway rw) {
        Element rwElem = doc.createElement("runway");

        int unitPart = 0, numericPart=1;
        String unitNumeric[];
        //TODO: Add reciprocal number for this runway
        rwElem.setAttribute("id", "r" + rw.getNumber() + "-" + (Integer.parseInt(rw.getNumber()) + 18));

        //Coord
        rwElem.appendChild(createCoordElem(doc, String.valueOf(rw.getLat()), String.valueOf(rw.getLon()), alt));

        //Length
        //TODO: Verify length unit
        unitNumeric = Utility.unitAndNumber(rw.getLength());
        Element length = Utility.createElemTextInside(doc, "length", unitNumeric[numericPart]);
        length.setAttribute("lengthUnit", unitNumeric[unitPart]);
        rwElem.appendChild(length);

        //Width
        //TODO: Verify width unit
        unitNumeric = Utility.unitAndNumber(rw.getWidth());
        Element width = Utility.createElemTextInside(doc, "width", unitNumeric[numericPart]);
        width.setAttribute("lengthUnit", unitNumeric[unitPart]);
        rwElem.appendChild(width);

        //Surface
        rwElem.appendChild(Utility.createElemTextInside(doc, "surface", rw.getSurface()));

        //Base End
        Element baseEnd = doc.createElement("baseEnd");
        baseEnd.appendChild(Utility.createElemTextInside(doc, "designation", rw.getNumber()));
        Element[] rwpoints = findRunwayPoints(doc, rw);
        baseEnd.appendChild(rwpoints[0]);

        Element heading = Utility.createElemTextInside(doc, "heading", String.valueOf(rw.getHeading()));
        heading.setAttribute("headingType", "true");
        baseEnd.appendChild(heading);

        baseEnd.appendChild(rwpoints[1]);
        rwElem.appendChild(baseEnd);

        //reciprocalEnd
        Element reciprocalEnd = doc.createElement("reciprocalEnd");
        reciprocalEnd.appendChild(Utility.createElemTextInside(doc, "designation", String.valueOf(Integer.parseInt(rw.getNumber()) + 18)));
        reciprocalEnd.appendChild(rwpoints[2]);

        Element heading2 = Utility.createElemTextInside(doc, "heading", String.valueOf(rw.getHeading() + 180));
        heading2.setAttribute("headingType", "true");
        reciprocalEnd.appendChild(heading2);
        reciprocalEnd.appendChild(rwpoints[3]);
        rwElem.appendChild(reciprocalEnd);

        return rwElem;
    }

    /**
     *
     * @param doc
     * @param rw
     * @return
     */
    private Element[] findRunwayPoints(Document doc, Runway rw) {
        Element[] rwpoints = new Element[4];
        Element startpoint = doc.createElement("startpoint");
        Element endpoint = doc.createElement("endpoint");
        Element startpoint1 = doc.createElement("startpoint");
        Element endpoint1 = doc.createElement("endpoint");
        Integer startInt = null;
        RelationshipEdge re = null;

        Set<RelationshipEdge> edgeSet = graph.edgeSet();

        for (RelationshipEdge edge : edgeSet) {

            if (!edge.getLabel().equalsIgnoreCase("RUNWAY") || !edge.getNumber().equalsIgnoreCase(rw.getNumber())) {
                continue;
            }

            boolean notStart = false;

            Integer edgeStartPoint = (Integer) edge.getV1();
            Set<RelationshipEdge> inEdges = graph.incomingEdgesOf(edgeStartPoint);

            re = edge;

            if (!inEdges.isEmpty()) {

                for (RelationshipEdge in : inEdges) {

                    if (in.getLabel().equalsIgnoreCase("RUNWAY") && in.getNumber().equals(rw.getNumber())) {
                        notStart = true;
                        break;
                    }

                }
            }

            if (notStart) {
                startInt = null;
            } else {
                startInt = edgeStartPoint;
                break;
            }
        }

        TaxiwayPoint startTWP = twPointMap.get(startInt);

        int backwardsNr = (re != null) ? Integer.parseInt(re.getNumber()) + 18 : -1;
        String forward = (re != null) ? "r" + re.getNumber() + "-" + backwardsNr : "";
        String backwards = (re != null) ? "r" + backwardsNr + "-" + re.getNumber() : "";

        startpoint.appendChild(createCoordElem(doc, String.valueOf(startTWP.getLat()), String.valueOf(startTWP.getLon()), this.alt));
        startpoint.appendChild(getConnectsToElem(doc, startInt, "", forward, false));
        rwpoints[0] = startpoint;

        startpoint1.appendChild(createCoordElem(doc, String.valueOf(startTWP.getLat()), String.valueOf(startTWP.getLon()), this.alt));
        startpoint1.appendChild(getConnectsToElem(doc, startInt, "", forward, false));
        rwpoints[2] = startpoint1;

        boolean foundEnd = false;
        Integer endInt = startInt;

        do {

            Set<RelationshipEdge> outEdges = graph.outgoingEdgesOf(endInt);

            if (outEdges.isEmpty()) {
                foundEnd = true;
            } else {

                for (RelationshipEdge edge : outEdges) {
                    if (edge.getLabel().equalsIgnoreCase("RUNWAY") && edge.getNumber().equalsIgnoreCase(rw.getNumber())) {
                        endInt = (Integer) edge.getV2();
                    }
                }

                //TODO: Not all possible cases are covered
            }
        } while (!foundEnd);

        TaxiwayPoint endTWP = twPointMap.get(endInt);

        endpoint.appendChild(createCoordElem(doc, String.valueOf(endTWP.getLat()), String.valueOf(endTWP.getLon()), this.alt));
        endpoint.appendChild(getConnectsToElem(doc, startInt, "", forward, false));
        rwpoints[1] = endpoint;

        endpoint1.appendChild(createCoordElem(doc, String.valueOf(endTWP.getLat()), String.valueOf(endTWP.getLon()), this.alt));
        endpoint1.appendChild(getConnectsToElem(doc, startInt, "", forward, false));
        rwpoints[3] = endpoint1;

        return rwpoints;
    }

    /**
     * fills the element taxiways of an airport
     */
    public Element taxiways(Document doc) {

        Element taxiways = doc.createElement("taxiways");

        ArrayList<RelationshipEdge> edges = getAllTaxiways();

        for (RelationshipEdge e : edges) {

            if (isStartOfTw(String.valueOf(e.getName()), getV1(e))) {
                Element taxiway = doc.createElement("taxiway");
                taxiway.setAttribute("id", "x" + e.getName());

                String desig = taxiNameMap.get(e.getName()).getName();
                taxiway.appendChild(Utility.createElemTextInside(doc, "designation", "Taxiway " + desig));
                taxiway.appendChild(Utility.createElemTextInside(doc, "surface", e.getSurface()));
                
                String unitNr[] = Utility.unitAndNumber(e.getWidth());
                Element width = Utility.createElemTextInside(doc, "width", unitNr[1]);
                width.setAttribute("lengthUnit", unitNr[0]);
                taxiway.appendChild(width);

                //Path
                Element path = doc.createElement("path");

                //Startpoint
                Element startpoint = doc.createElement("startpoint");
                TaxiwayPoint twp = twPointMap.get((Integer) e.getV1());
                startpoint.appendChild(createCoordElem(doc, String.valueOf(twp.getLat()), String.valueOf(twp.getLat()), this.alt));
                startpoint.appendChild(getConnectsToElem(doc, getV1(e), String.valueOf(e.getName()), "", false));
                path.appendChild(startpoint);

                if (MainParser.devMode) {
                    System.out.println("============================================");
                    System.out.println("START: " + e.getV1());
                }

                RelationshipEdge nextEdge = e;
                while (!isLastOfTw(String.valueOf(e.getName()), getV2(nextEdge))) {

                    //Midpoint
                    Element midpoint = doc.createElement("midpoint");
                    twp = twPointMap.get(getV2(nextEdge));
                    midpoint.appendChild(createCoordElem(doc, String.valueOf(twp.getLat()), String.valueOf(twp.getLat()), this.alt));
                    midpoint.appendChild(getConnectsToElem(doc, getV1(e), String.valueOf(e.getName()), "", false));
                    path.appendChild(midpoint);

                    if (MainParser.devMode) {
                        System.out.println("MID: " + getV2(nextEdge));
                    }

                    nextEdge = getNext(nextEdge);
                }

                //Endpoint
                Element endpoint = doc.createElement("endpoint");
                twp = twPointMap.get((Integer) nextEdge.getV2());
                endpoint.appendChild(createCoordElem(doc, String.valueOf(twp.getLat()), String.valueOf(twp.getLat()), this.alt));
                endpoint.appendChild(getConnectsToElem(doc, getV1(e), String.valueOf(e.getName()), "", false));
                path.appendChild(endpoint);

                if (MainParser.devMode) {
                    System.out.println("END: " + getV2(nextEdge));
                    System.out.println("============================================");
                }
                
                taxiway.appendChild(path);

                taxiways.appendChild(taxiway);
            }
        }

        return taxiways;
    }

    /**
     * conversion from obj to int
     *
     * @param e
     * @return
     */
    public int getV1(RelationshipEdge e) {
        return (int) e.getV1();
    }

    /**
     *
     * @param e
     * @return
     */
    public int getV2(RelationshipEdge e) {
        return (int) e.getV2();
    }

    /**
     * returns if point is the initial point of the taxiway or not
     *
     * @param name
     * @param point
     * @return true if is the initial point of a taxiway with name 'name'
     */
    public boolean isStartOfTw(String name, int point) {

        boolean isStart = true;

        Set<RelationshipEdge> res = graph.incomingEdgesOf(point);

        for (RelationshipEdge e : res) {

            if (e.getLabel().equalsIgnoreCase("TAXI")) {
                if (String.valueOf(e.getName()).equalsIgnoreCase(name)) {
                    isStart = false;
                    break;
                }
            }
        }

        return isStart;
    }

    /**
     * returns if point is the final point of the taxiway or not
     *
     * @param name
     * @param point
     * @return true if is the final point of a taxiway with name 'name'
     */
    public boolean isLastOfTw(String name, int point) {

        boolean isEnd = true;

        Set<RelationshipEdge> res = graph.outgoingEdgesOf(point);

        for (RelationshipEdge e : res) {

            if (e.getLabel().equalsIgnoreCase("TAXI")) {
                if (String.valueOf(e.getName()).equalsIgnoreCase(name)) {
                    isEnd = false;
                    break;
                }
            }
        }

        return isEnd;
    }

    /**
     * Must not receive an ending vertex
     *
     * @param e
     * @return
     */
    public RelationshipEdge getNext(RelationshipEdge e) {

        String name = String.valueOf(e.getName());
        RelationshipEdge next = e;

        for (RelationshipEdge e2 : graph.outgoingEdgesOf((int) e.getV2())) {
            if (e2.getLabel().equalsIgnoreCase("TAXI")) {
                if (String.valueOf(e2.getName()).equalsIgnoreCase(name)) {
                    return next = e2;
                }
            }
        }

        return next;

    }

    /**
     * gets all taxiways from a given point
     *
     * @param startingPoint
     * @return
     */
    Set<RelationshipEdge> getAllTaxiwaysFrom(int startingPoint) {

        Set<RelationshipEdge> taxiways = graph.edgesOf(startingPoint);

        for (RelationshipEdge e : taxiways) {
            if (!e.getLabel().equalsIgnoreCase("TAXI")) {
                taxiways.remove(e);
            }
        }

        return taxiways;
    }

    /**
     * returns all taxiways
     *
     * @return taxiways
     */
    ArrayList<RelationshipEdge> getAllTaxiways() {
        Set<RelationshipEdge> all = graph.edgeSet();
        ArrayList<RelationshipEdge> taxiways = new ArrayList<>();
        for (RelationshipEdge e : all) {
            if (e.getLabel().equalsIgnoreCase("TAXI")) {
                taxiways.add(e);
            }
        }

        return taxiways;
    }

    private Element parkingSpaces(Document doc) {
        Element parkingSpaces = doc.createElement("parkingSpaces");

        for (TaxiwayPath path : twPathList) {

            if (path.getType().equalsIgnoreCase("PARKING")) {

                Element parking = doc.createElement("parking");
                TaxiwayParking twpark = twParkingMap.get(path.getEnd());
                parking.setAttribute("parkingType", twpark.getType());
                parking.setAttribute("id", "p" + twpark.getNumber());
                parking.appendChild(Utility.createElemTextInside(doc, "designation", "XXX designation XXX"));
                parking.appendChild(Utility.createElemTextInside(doc, "description", "XXX description XXX"));

                if (twpark.getAirlineCodes() != null) {
                    parking.appendChild(Utility.createElemTextInside(doc, "airlines", twpark.getAirlineCodes()));
                } else {
                    parking.appendChild(Utility.createElemTextInside(doc, "airlines", ""));
                }

                parking.appendChild(createCoordElem(doc, String.valueOf(twpark.getLat()), String.valueOf(twpark.getLon()), this.alt));

                
                String unitNr[] = Utility.unitAndNumber(twpark.getRadius());
                Element radius = Utility.createElemTextInside(doc, "radius", unitNr[1]);
                radius.setAttribute("lengthUnit", unitNr[0]);
                parking.appendChild(radius);

                Element connectsToTw = doc.createElement("connectsToTaxiway");
                connectsToTw.setAttribute("xway", "XXX");
                TaxiwayPoint twp = twPointMap.get(path.getStart());
                connectsToTw.appendChild(createCoordElem(doc, String.valueOf(twp.getLat()), String.valueOf(twp.getLon()), this.alt));
                parking.appendChild(getConnectsToElem(doc, path.getStart(), "-1", "-1", true));

                parkingSpaces.appendChild(parking);

            }

        }

        return parkingSpaces;
    }

    public ArrayList<ArrayList<String>> getConnectsTo(int point) {

        Set<RelationshipEdge> es = graph.edgesOf(point);
        ArrayList<String> xConnectsTo = new ArrayList<>();
        ArrayList<String> rConnectsTo = new ArrayList<>();

        ArrayList<ArrayList<String>> connectsTo = new ArrayList<>();

        for (RelationshipEdge e : es) {
            String nameOf;
            if (e.getLabel().equalsIgnoreCase("TAXI")) {

                nameOf = "x" + String.valueOf(e.getName());

                if (xConnectsTo.indexOf(nameOf) == -1) {
                    xConnectsTo.add(nameOf);
                }
            } else if (e.getLabel().equalsIgnoreCase("RUNWAY")) {
                int val = Integer.parseInt(e.getNumber());
                val += 18;

                nameOf = "r" + e.getNumber() + "-" + val;

                if (rConnectsTo.indexOf(nameOf) == -1) {
                    rConnectsTo.add(nameOf);
                }
            }
        }

        connectsTo.add(xConnectsTo);
        connectsTo.add(rConnectsTo);

        return connectsTo;

    }

    public Element getConnectsToElem(Document doc, int point, String currenttwName, String curRwName, boolean onlyTws) {
        Element connectsTo = doc.createElement("connectsTo");
        ArrayList<ArrayList<String>> names = getConnectsTo(point);

        ArrayList<String> xways = names.get(0);
        ArrayList<String> rways = names.get(1);

        currenttwName = "x" + currenttwName;

        for (String xway : names.get(0)) {

            if (xway.equalsIgnoreCase(currenttwName)) {
                continue;
            }
            Element xwayElem = doc.createElement("xway");
            xwayElem.setAttribute("idr", xway);
            connectsTo.appendChild(xwayElem);
        }

        if (!onlyTws) {
            for (String rway : names.get(1)) {
                if (rway.equalsIgnoreCase(curRwName)) {
                    continue;
                }
                Element rwayElem = doc.createElement("rway");
                rwayElem.setAttribute("idr", rway);
                connectsTo.appendChild(rwayElem);
            }
        }

        return connectsTo;

    }
}
