package elem;

import java.util.HashSet;

public class ApproachLights {

    private String end;
    private String system;
    private int strobes;
    private String reil;
    private String touchdown;
    private String endLights;

    private boolean endSet;
    private boolean systemSet;
    private boolean strobesSet;
    private boolean reilSet;
    private boolean touchdownSet;
    private boolean endLightsSet;

    private final static HashSet<String> systemValues;

    static {
        systemValues = new HashSet<>();
        systemValues.add("NONE");
        systemValues.add("ALSF1");
        systemValues.add("ALSF2");
        systemValues.add("CALVERT");
        systemValues.add("CALVERT2");
        systemValues.add("MALS");
        systemValues.add("MALSF");
        systemValues.add("MALSR");
        systemValues.add("ODALS");
        systemValues.add("RAIL");
        systemValues.add("SALS");
        systemValues.add("SALSF");
        systemValues.add("SSALF");
        systemValues.add("SSALR");
        systemValues.add("SSALS");
    }

    public ApproachLights() {
        endSet = false;
        systemSet = false;
        strobesSet = false;
        reilSet = false;
        touchdownSet = false;
        endLightsSet = false;

        strobes = Integer.MAX_VALUE;
    }

    public void print() {
        System.out.println("### ApproachLights - Attributes ###");

        if (endSet) {
            System.out.println("End = " + end);
        }

        if (systemSet) {
            System.out.println("System = " + system);
        }

        if (strobesSet) {
            System.out.println("Strobes = " + strobes);
        }

        if (reilSet) {
            System.out.println("Reil = " + reil);
        }

        if (touchdownSet) {
            System.out.println("Touchdown = " + touchdown);
        }

        if (endLightsSet) {
            System.out.println("EndLights = " + endLights);
        }

        System.out.print(System.lineSeparator());
    }

    /**
     *
     * @throws IllegalArgumentException
     */
    public void checkMissingAttr() throws IllegalArgumentException {
        Utility.checkMandatoryString("End", end);
    }

    /**
     *
     * @param input
     * @throws IllegalArgumentException
     */
    public void setEnd(String input) throws IllegalArgumentException {

        if (Utility.checkDuplicString(end, "End")) {
            String inputStr = input.replaceAll("^\"|\"$", "");
            if (inputStr.equals("PRIMARY") || inputStr.equals("SECONDARY")) {
                endSet = true;
                end = inputStr;
            } else {
                throw new IllegalArgumentException("End: Invalid value. Supported values are PRIMARY and SECONDARY.");
            }
        }
    }

    /**
     *
     * @param input
     * @throws IllegalArgumentException
     */
    public void setSystem(String input) throws IllegalArgumentException {

        if (Utility.checkDuplicString(system, "System")) {
            String inputStr = input.replaceAll("^\"|\"$", "");

            if (systemValues.contains(inputStr)) {
                systemSet = true;
                system = inputStr;
            } else {
                throw new IllegalArgumentException("System: Invalid value.");
            }
        }
    }

    /**
     *
     * @param input
     * @throws IllegalArgumentException
     */
    public void setStrobes(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicInt(strobes, "Strobes")) {
            String inputStr = input.replaceAll("^\"|\"$", "");
            int tmp = Integer.parseInt(inputStr);

            if (tmp >= 0) {
                strobesSet = true;
                strobes = tmp;
            } else {
                throw new IllegalArgumentException("Strobes: Invalid value. Must be a non-negative integer.");
            }
        }
    }

    /**
     *
     * @param input
     * @throws IllegalArgumentException
     */
    public void setReil(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(reil, "Reil")) {
            String inputStr = input.replaceAll("^\"|\"$", "");
            if (inputStr.equals("TRUE") || inputStr.equals("FALSE")) {
                reilSet = true;
                reil = inputStr;
            } else {
                throw new IllegalArgumentException("Reil: Valid values are TRUE or FALSE.");
            }
        }
    }

    /**
     *
     * @param input
     * @throws IllegalArgumentException
     */
    public void setTouchdown(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(touchdown, "Touchdown")) {
            String inputStr = input.replaceAll("^\"|\"$", "");
            if (inputStr.equals("TRUE") || inputStr.equals("FALSE")) {
                touchdownSet = true;
                touchdown = inputStr;
            } else {
                throw new IllegalArgumentException("Touchdown: Valid values are TRUE or FALSE.");
            }
        }
    }

    /**
     *
     * @param input
     * @throws IllegalArgumentException
     */
    public void setEndLights(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(endLights, "EndLights")) {
            String inputStr = input.replaceAll("^\"|\"$", "");
            if (inputStr.equals("TRUE") || inputStr.equals("FALSE")) {
                endLightsSet = true;
                endLights = inputStr;
            } else {
                throw new IllegalArgumentException("End Lights: Valid values are TRUE or FALSE.");
            }
        }
    }
}
