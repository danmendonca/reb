package elem;

import java.util.ArrayList;
import java.util.HashSet;

public class Runway {

    private Double lat;
    private Double lon;
    private String alt;
    private String surface;
    private Double heading;
    private String length;

    private String width;
    private String number;
    private String designator;
    private String primaryDesignator;
    private String secondaryDesignator;
    private String patternAltitude;
    private String primaryTakeoff;
    private String primaryLanding;
    private String primaryPattern;
    private String secondaryTakeoff;
    private String secondaryLanding;
    private String secondaryPattern;
    private String primaryMarkingBias;
    private String secondaryMarkingBias;

    private final static HashSet<String> surfaceSet;
    private final static HashSet<String> designatorSet;

    static {
        surfaceSet = new HashSet<>();
        surfaceSet.add("ASPHALT");
        surfaceSet.add("BITUMINOUS");
        surfaceSet.add("BRICK");
        surfaceSet.add("CLAY");
        surfaceSet.add("CEMENT");
        surfaceSet.add("CONCRETE");
        surfaceSet.add("CORAL");
        surfaceSet.add("DIRT");
        surfaceSet.add("GRASS");
        surfaceSet.add("GRAVEL");
        surfaceSet.add("ICE");
        surfaceSet.add("MACADAM");
        surfaceSet.add("OIL_TREATED");
        surfaceSet.add("PLANKS");
        surfaceSet.add("SAND");
        surfaceSet.add("SHALE");
        surfaceSet.add("SNOW");
        surfaceSet.add("STEEL_MATS");
        surfaceSet.add("TARMAC");
        surfaceSet.add("UNKNOWN");
        surfaceSet.add("WATER");

        designatorSet = new HashSet<>();
        designatorSet.add("NONE");
        designatorSet.add("C");
        designatorSet.add("CENTER");
        designatorSet.add("L");
        designatorSet.add("LEFT");
        designatorSet.add("R");
        designatorSet.add("RIGHT");
        designatorSet.add("W");
        designatorSet.add("WATER");
        designatorSet.add("A");
        designatorSet.add("B");
    }

    private Markings markingsElem;
    private Lights lightsElem;
    private ArrayList<OffsetThreshold> offSetThresholdList;
    private BlastPad blastPadElem;
    private Overrun overRunElem;
    private ArrayList<ApproachLights> approachLightsList;
    private ArrayList<Vasi> vasiList;
    private RunwayStart runwayStartElem;
    private Ils ilsElem;

    public Runway() {
        lat = Double.NaN;
        lon = Double.NaN;
        heading = Double.NaN;

        markingsElem = new Markings();
        lightsElem = new Lights();
        offSetThresholdList = new ArrayList<>();
        blastPadElem = new BlastPad();
        overRunElem = new Overrun();
        approachLightsList = new ArrayList<>();
        vasiList = new ArrayList<>();
        runwayStartElem = new RunwayStart();
        ilsElem = new Ils();
    }

    public void setMarkings(Markings m) {
        markingsElem = m;
    }

    public void setLights(Lights l) {
        lightsElem = l;
    }

    public void setOffSetThreshold(OffsetThreshold o) {
        offSetThresholdList.add(o);
    }

    public void setBlastPad(BlastPad b) {
        blastPadElem = b;
    }

    public void setOverRun(Overrun o) {
        overRunElem = o;
    }

    public void setApproachLight(ApproachLights a) {
        approachLightsList.add(a);
    }

    public void setVasi(Vasi v) {
        vasiList.add(v);
    }

    public void setRunwayStart(RunwayStart r) {
        runwayStartElem = r;
    }

    public void setIls(Ils i) {
        ilsElem = i;
    }

    /**
     *
     */
    public void print() {
        System.out.println("### Runway - Attributes ###");
        Utility.printDouble("Lat", lat);
        Utility.printDouble("Lon", lon);
        Utility.printString("Alt", alt);
        Utility.printString("Surface", surface);
        Utility.printDouble("Heading", heading);
        Utility.printString("Length", length);
        Utility.printString("Width", width);
        Utility.printString("Number", number);
        Utility.printString("Designator", designator);
        Utility.printString("Primary Designator", primaryDesignator);
        Utility.printString("Secondary Designator", secondaryDesignator);
        Utility.printString("Pattern Altitude", patternAltitude);
        Utility.printString("Primary Takeoff", primaryTakeoff);
        Utility.printString("Primary Landing", primaryLanding);
        Utility.printString("Primary Pattern", primaryPattern);
        Utility.printString("Secondary Takeoff", secondaryTakeoff);
        Utility.printString("Secondary Landing", secondaryLanding);
        Utility.printString("Secondary Pattern", secondaryPattern);
        Utility.printString("Primary Marking Bias", primaryMarkingBias);
        Utility.printString("Secondary Marking Bias", secondaryMarkingBias);
        System.out.print(System.lineSeparator());

        markingsElem.print();
        lightsElem.print();

        for (OffsetThreshold o : offSetThresholdList) {
            o.print();
        }

        blastPadElem.print();
        overRunElem.print();

        for (ApproachLights a : approachLightsList) {
            a.print();
        }

        for (Vasi v : vasiList) {
            v.print();
        }

        runwayStartElem.print();

        ilsElem.print();
    }

    /**
     *
     * @throws IllegalArgumentException
     */
    public void checkMissingAttr() throws IllegalArgumentException {
        Utility.checkMandatoryDouble("Lat", lat);
        Utility.checkMandatoryDouble("Lon", lon);
        Utility.checkMandatoryString("Alt", alt);
        Utility.checkMandatoryString("Surface", surface);
        Utility.checkMandatoryDouble("Heading", heading);
        Utility.checkMandatoryString("Length", length);
        Utility.checkMandatoryString("Width", width);
        Utility.checkMandatoryString("Number", number);
    }

    /**
     *
     * @param input
     */
    public void setLat(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicDouble(lat, "Lat")) {
            String str = input.replaceAll("^\"|\"$", "");
            lat = Double.parseDouble(str);

            if ((this.lat < -90) || (this.lat > 90)) {
                this.lat = Double.NaN;
                throw new IllegalArgumentException("Lat: -90 to +90 degrees");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setLon(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicDouble(lon, "Lon")) {
            String str = input.replaceAll("^\"|\"$", "");
            lon = Double.parseDouble(str);

            if ((this.lon < -180) || (this.lon > 180)) {
                this.lon = Double.NaN;
                throw new IllegalArgumentException("Lon: -180 to +180 degrees");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setAlt(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(alt, "Alt")) {
            alt = Utility.setMeterFeet(input, "Alt");
        }
    }

    /**
     *
     * @param input
     */
    public void setSurface(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(surface, "Surface")) {
            String surfaceStr = input.replaceAll("^\"|\"$", "");

            if (surfaceSet.contains(surfaceStr)) {
                surface = surfaceStr;
            } else {
                throw new IllegalArgumentException("Surface: Invalid surface");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setHeading(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicDouble(heading, "Heading")) {
            String headingStr = input.replaceAll("^\"|\"$", "");
            heading = Double.parseDouble(headingStr);

            if ((heading < 0.0) || (heading > 360.0)) {
                heading = Double.NaN;
                throw new IllegalArgumentException("Heading: 0.0 to 360.0");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setLength(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(length, "Length")) {
            length = Utility.setMeterFeet(input, "Length");
        }
    }

    /**
     *
     * @param input
     */
    public void setWidth(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(width, "Width")) {
            width = Utility.setMeterFeet(input, "Width");
        }
    }

    /**
     *
     * @param input
     */
    public void setNumber(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(number, "Number")) {
            number = input.replaceAll("^\"|\"$", "");
        }
    }

    /**
     *
     * @param input
     */
    public void setDesignator(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(designator, "Designatior")) {
            String str = input.replaceAll("^\"|\"$", "");

            if (designatorSet.contains(str)) {
                designator = str;
            } else {
                throw new IllegalArgumentException("Designator: Invalid value");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setPrimDesignator(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(primaryDesignator, "Primary Designator")) {
            String str = input.replaceAll("^\"|\"$", "");

            if (designatorSet.contains(str)) {
                primaryDesignator = str;
            } else {
                throw new IllegalArgumentException("Designator: Invalid value");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setSecondDesignator(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(secondaryDesignator, "Secondary Designator")) {
            String str = input.replaceAll("^\"|\"$", "");

            if (designatorSet.contains(str)) {
                secondaryDesignator = str;
            } else {
                throw new IllegalArgumentException("Primary Designator: Invalid value");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setPatternAltitude(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(patternAltitude, "Pattern Altitude")) {
            patternAltitude = Utility.setMeterFeet(input, "Pattern Altitude");
        }
    }

    /**
     *
     * @param input
     */
    public void setPrimaryTakeOff(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(primaryTakeoff, "Primary Takeoff")) {
            String str = input.replaceAll("^\"|\"$", "");

            switch (str) {
                case "TRUE":
                    primaryTakeoff = "TRUE";
                    break;
                case "YES":
                    primaryTakeoff = "TRUE";
                    break;
                case "FALSE":
                    primaryTakeoff = "FALSE";
                    break;
                case "NO":
                    primaryTakeoff = "FALSE";
                    break;
                case "":
                    primaryTakeoff = "TRUE";
                    break;
                default:
                    throw new IllegalArgumentException("Primary Takeoff: Invalid value");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setPrimaryLanding(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(primaryLanding, "Primary Landing")) {
            String str = input.replaceAll("^\"|\"$", "");

            switch (str) {
                case "TRUE":
                    primaryLanding = "TRUE";
                    break;
                case "YES":
                    primaryLanding = "TRUE";
                    break;
                case "FALSE":
                    primaryLanding = "FALSE";
                    break;
                case "NO":
                    primaryLanding = "FALSE";
                    break;
                case "":
                    primaryLanding = "TRUE";
                    break;
                default:
                    throw new IllegalArgumentException("Primary Landing: Invalid value");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setPrimaryPattern(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(primaryPattern, "Primary Pattern")) {
            String str = input.replaceAll("^\"|\"$", "");

            switch (str) {
                case "LEFT":
                    primaryPattern = "LEFT";
                    break;
                case "RIGHT":
                    primaryPattern = "RIGHT";
                    break;
                case "":
                    primaryPattern = "LEFT";
                    break;
                default:
                    throw new IllegalArgumentException("Primary Pattern: Invalid value");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setSecondTakeOff(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(secondaryTakeoff, "Secondary Takeoff")) {
            String str = input.replaceAll("^\"|\"$", "");

            switch (str) {
                case "TRUE":
                    secondaryTakeoff = "TRUE";
                    break;
                case "YES":
                    secondaryTakeoff = "TRUE";
                    break;
                case "FALSE":
                    secondaryTakeoff = "FALSE";
                    break;
                case "NO":
                    secondaryTakeoff = "FALSE";
                    break;
                case "":
                    secondaryTakeoff = "TRUE";
                    break;
                default:
                    throw new IllegalArgumentException("Secondary Takeoff: Invalid value");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setSecondLanding(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(secondaryLanding, "Secondary Landing")) {
            String str = input.replaceAll("^\"|\"$", "");

            switch (str) {
                case "TRUE":
                    secondaryLanding = "TRUE";
                    break;
                case "YES":
                    secondaryLanding = "TRUE";
                    break;
                case "FALSE":
                    secondaryLanding = "FALSE";
                    break;
                case "NO":
                    secondaryLanding = "FALSE";
                    break;
                case "":
                    secondaryLanding = "TRUE";
                    break;
                default:
                    throw new IllegalArgumentException("Secondary Landing: Invalid value");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setSecondPattern(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(secondaryPattern, "Secondary Pattern")) {
            String str = input.replaceAll("^\"|\"$", "");

            switch (str) {
                case "LEFT":
                    secondaryPattern = "LEFT";
                    break;
                case "RIGHT":
                    secondaryPattern = "RIGHT";
                    break;
                case "":
                    secondaryPattern = "LEFT";
                    break;
                default:
                    throw new IllegalArgumentException("Secondary Pattern: Invalid value");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setPrimaryMarkingBias(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(primaryMarkingBias, "Primary Marking Bias")) {
            primaryMarkingBias = Utility.setNauticalMeterFeet(input, "Primary Marking Bias");
        }
    }

    /**
     *
     * @param input
     */
    public void setSecondMarkingBias(String input) throws IllegalArgumentException {
        if(Utility.checkDuplicString(secondaryMarkingBias, "Secondary Marking Bias")){
            secondaryMarkingBias = Utility.setNauticalMeterFeet(input, "Secondary Marking Bias");
        }
    }

    public String getNumber() {
        return number;
    }

    public Double getLat() {
        return lat;
    }

    public Double getLon() {
        return lon;
    }

    public String getAlt() {
        return alt;
    }

    public String getLength() {
        return length;
    }

    public String getWidth() {
        return width;
    }

    public String getSurface() {
        return surface;
    }

    public Double getHeading() {
        return heading;
    }
    
    

    
}
