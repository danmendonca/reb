package elem;

public class Fuel {

    private String type;
    private String availability;

    public Fuel() {
    }

    /**
     *
     */
    public void print() {

        System.out.println("### Fuel - Attributes ###");

        if ((this.type != null) && !this.type.isEmpty()) {
            System.out.println("Type = " + this.type);
        }

        if ((this.availability != null) && !this.availability.isEmpty()) {
            System.out.println("Availability = " + this.availability);
        }

        System.out.print(System.lineSeparator());

    }

    /**
     *
     * @throws IllegalArgumentException
     */
    public void checkMissingAttr() throws IllegalArgumentException {
        Utility.checkMandatoryString("Type", type);
        Utility.checkMandatoryString("Availability", availability);
    }

    /**
     *
     * @param input
     */
    public void setType(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(type, "Type")) {
            String typeStr = input.replaceAll("^\"|\"$", "");

            switch (typeStr) {
                case "73":
                    this.type = "73";
                    break;
                case "87":
                    this.type = "87";
                    break;
                case "100":
                    this.type = "100";
                    break;
                case "130":
                    this.type = "130";
                    break;
                case "145":
                    this.type = "145";
                    break;
                case "MOGAS":
                    this.type = "MOGAS";
                    break;
                case "JET":
                    this.type = "JET";
                    break;
                case "JETA":
                    this.type = "JETA";
                    break;
                case "JETA1":
                    this.type = "JETA1";
                    break;
                case "JETAP":
                    this.type = "JETAP";
                    break;
                case "JETB":
                    this.type = "JETB";
                    break;
                case "JET4":
                    this.type = "JET4";
                    break;
                case "JET5":
                    this.type = "JET5";
                    break;
                case "UNKNOWN":
                    this.type = "UNKNOWN";
                    break;
                default:
                    throw new IllegalArgumentException("Type: Invalid type");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setAvailability(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(availability, "Availability")) {
            String availabilityStr = input.replaceAll("^\"|\"$", "");

            switch (availabilityStr) {
                case "YES":
                    this.availability = "YES";
                    break;
                case "NO":
                    this.availability = "NO";
                    break;
                case "UNKNOWN":
                    this.availability = "UNKNOWN";
                    break;
                case "PRIOR_REQUEST":
                    this.availability = "PRIOR_REQUEST";
                    break;
                default:
                    throw new IllegalArgumentException("Availability: Invalid Availability");
            }
        }
    }
}
