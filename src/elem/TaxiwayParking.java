package elem;

public class TaxiwayParking {

    private int index;
    private boolean indexSet;
    private int number;
    private boolean numberSet;

    private double lat;
    private double lon;
    private double biasX;
    private double biasZ;
    private double heading;
    private double teeOffset1;
    private double teeOffset2;
    private double teeOffset3;
    private double teeOffset4;

    private String type;
    private String name;
    private String airlineCodes;
    private String pushBack;
    private String radius;

    public TaxiwayParking() {
        lat = Double.NaN;
        lon = Double.NaN;
        biasX = Double.NaN;
        biasZ = Double.NaN;
        heading = Double.NaN;
        teeOffset1 = Double.NaN;
        teeOffset2 = Double.NaN;
        teeOffset3 = Double.NaN;
        teeOffset4 = Double.NaN;

        index = Integer.MAX_VALUE;
        indexSet = false;
        number = Integer.MAX_VALUE;
        numberSet = false;
    }

    public int getIndex() {
        return index;
    }

    /**
     *
     */
    public void print() {

        System.out.println("### TaxiwayParking - Attributes ###");

        if (indexSet) {
            System.out.println("Index = " + this.index);
        }

        if (numberSet) {
            System.out.println("Number = " + this.number);
        }

        if (!Double.isNaN(lat)) {
            System.out.println("Lat = " + this.lat);
        }

        if (!Double.isNaN(lon)) {
            System.out.println("Lon = " + this.lon);
        }

        if (!Double.isNaN(biasX)) {
            System.out.println("Bias X = " + this.biasX);
        }

        if (!Double.isNaN(biasZ)) {
            System.out.println("Bias Z = " + this.biasZ);
        }

        if (!Double.isNaN(heading)) {
            System.out.println("Heading = " + heading);
        }

        if (!Double.isNaN(teeOffset1)) {
            System.out.println("teeOffSet1 = " + teeOffset1);
        }

        if (!Double.isNaN(teeOffset2)) {
            System.out.println("teeOffSet2 = " + teeOffset2);
        }

        if (!Double.isNaN(teeOffset3)) {
            System.out.println("teeOffSet3 = " + teeOffset3);
        }

        if (!Double.isNaN(teeOffset4)) {
            System.out.println("teeOffSet4 = " + teeOffset4);
        }

        if ((this.type != null) && !this.type.isEmpty()) {
            System.out.println("Type = " + this.type);
        }

        if ((name != null) && !name.isEmpty()) {
            System.out.println("Name = " + name);
        }

        if ((airlineCodes != null) && !airlineCodes.isEmpty()) {
            System.out.println("AirlineCodes = " + airlineCodes);
        }

        if ((pushBack != null) && !pushBack.isEmpty()) {
            System.out.println("PushBack = " + pushBack);
        }

        if ((radius != null) && !radius.isEmpty()) {
            System.out.println("Radius = " + radius);
        }

        System.out.print(System.lineSeparator());

    }

    /**
     *
     * @throws IllegalArgumentException
     */
    public void checkMissingAttr() throws IllegalArgumentException {
        Utility.checkMandatoryInt("Index", index);

        if ((Double.isNaN(lat) || Double.isNaN(lon)) && (Double.isNaN(biasX) || Double.isNaN(biasZ))) {
            throw new IllegalArgumentException("(Lat-Lon) or (BiasX-BiasZ) - Missing mandatory attribute");
        }

        Utility.checkMandatoryDouble("Heading", heading);
        Utility.checkMandatoryString("Radius", radius);
        Utility.checkMandatoryString("Type", type);
        Utility.checkMandatoryString("Name", name);
        Utility.checkMandatoryInt("Number", number);
        Utility.checkMandatoryString("PushBack", pushBack);
        //Utility.checkMandatoryDouble("teeOffSet1", teeOffset1);
        //Utility.checkMandatoryDouble("teeOffSet2", teeOffset2);
        //Utility.checkMandatoryDouble("teeOffSet3", teeOffset3);
        //Utility.checkMandatoryDouble("teeOffSet4", teeOffset4);
    }

    /**
     *
     * @param input
     */
    public void setIndex(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicInt(index, "Index")) {
            String indexStr = input.replaceAll("^\"|\"$", "");
            this.index = Integer.parseInt(indexStr);
            this.indexSet = true;

            if ((this.index < 0) || (this.index > 3999)) {
                this.indexSet = false;
                throw new IllegalArgumentException("Index: Valid integer range is 0 to 3999");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setNumber(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicInt(number, "Number")) {
            String numberStr = input.replaceAll("^\"|\"$", "");
            number = Integer.parseInt(numberStr);
            numberSet = true;

            if ((number < 0) || (number > 3999)) {
                numberSet = false;
                throw new IllegalArgumentException("Number: Valid integer range is 0 to 3999");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setLat(String input) throws NumberFormatException, IllegalArgumentException {
        if (!Double.isNaN(biasX) || !Double.isNaN(biasZ)) {
            throw new IllegalArgumentException("Can either have a lat-lon position, or a bias.");
        }
        if (Utility.checkDuplicDouble(lat, "Lat")) {
            lat = Utility.setDouble(input, "Lat", -90.0, 90.0);
        }
    }

    /**
     *
     * @param input
     */
    public void setLon(String input) throws NumberFormatException, IllegalArgumentException {
        if (!Double.isNaN(biasX) || !Double.isNaN(biasZ)) {
            throw new IllegalArgumentException("Can either have a lat-lon position, or a bias.");
        }
        if (Utility.checkDuplicDouble(lon, "Lon")) {
            lon = Utility.setDouble(input, "Lon", -180.0, 180.0);
        }
    }

    /**
     *
     * @param input
     */
    public void setBiasX(String input) throws NumberFormatException, IllegalArgumentException {
        if (!Double.isNaN(lat) || !Double.isNaN(lon)) {
            throw new IllegalArgumentException("Can either have a lat-lon position, or a bias");
        }
        if (Utility.checkDuplicDouble(biasX, "BiasX")) {
            biasX = Utility.setDouble(input, "Bias X", Double.MIN_VALUE, Double.MAX_VALUE);
        }
    }

    /**
     *
     * @param input
     */
    public void setBiasZ(String input) throws NumberFormatException, IllegalArgumentException {
        if (!Double.isNaN(lat) || !Double.isNaN(lon)) {
            throw new IllegalArgumentException("Can either have a lat-lon position, or a bias");
        }
        if (Utility.checkDuplicDouble(biasZ, "BiasZ")) {
            biasZ = Utility.setDouble(input, "Bias Z", Double.MIN_VALUE, Double.MAX_VALUE);
        }
    }

    /**
     *
     * @param input
     */
    public void setHeading(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicDouble(heading, "Heading")) {
            String headingStr = input.replaceAll("^\"|\"$", "");
            heading = Double.parseDouble(headingStr);

            if ((heading < 0.0) || (heading > 360.0)) {
                heading = Double.NaN;
                throw new IllegalArgumentException("Heading: 0.0 to 360.0");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setRadius(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicString(radius, "Radius")) {
            radius = Utility.setMeterFeet(input, "Radius");
        }
    }

    /**
     *
     * @param input
     */
    public void setTeeOffset1(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicDouble(teeOffset1, "teeOffSet1")) {
            String teeOffSetStr = input.replaceAll("^\"|\"$", "");
            teeOffset1 = Double.parseDouble(teeOffSetStr);

            if ((teeOffset1 < 0.1) || (teeOffset1 > 50.0)) {
                teeOffset1 = Double.NaN;
                throw new IllegalArgumentException("teeOffSet: 0.1 to 50.0");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setTeeOffset2(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicDouble(teeOffset2, "teeOffSet2")) {
            String teeOffSetStr = input.replaceAll("^\"|\"$", "");
            teeOffset2 = Double.parseDouble(teeOffSetStr);

            if ((teeOffset2 < 0.1) || (teeOffset2 > 50.0)) {
                teeOffset2 = Double.NaN;
                throw new IllegalArgumentException("teeOffSet: 0.1 to 50.0");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setTeeOffset3(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicDouble(teeOffset3, "teeOffSet3")) {
            String teeOffSetStr = input.replaceAll("^\"|\"$", "");
            teeOffset3 = Double.parseDouble(teeOffSetStr);

            if ((teeOffset3 < 0.1) || (teeOffset3 > 50.0)) {
                teeOffset3 = Double.NaN;
                throw new IllegalArgumentException("teeOffSet: 0.1 to 50.0");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setTeeOffset4(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicDouble(teeOffset4, "teeOffSet4")) {
            String teeOffSetStr = input.replaceAll("^\"|\"$", "");
            teeOffset4 = Double.parseDouble(teeOffSetStr);

            if ((teeOffset4 < 0.1) || (teeOffset4 > 50.0)) {
                teeOffset4 = Double.NaN;
                throw new IllegalArgumentException("teeOffSet: 0.1 to 50.0");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setType(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(type, "Type")) {
            String typeStr = input.replaceAll("^\"|\"$", "");

            switch (typeStr) {
                case "NONE":
                    this.type = "NONE";
                    break;
                case "DOCK_GA":
                    this.type = "DOCK_GA";
                    break;
                case "FUEL":
                    this.type = "FUEL";
                    break;
                case "GATE_HEAVY":
                    this.type = "GATE_HEAVY";
                    break;
                case "GATE_MEDIUM":
                    this.type = "GATE_MEDIUM";
                    break;
                case "GATE_SMALL":
                    this.type = "GATE_SMALL";
                    break;
                case "RAMP_CARGO":
                    this.type = "RAMP_CARGO";
                    break;
                case "RAMP_GA":
                    this.type = "RAMP_GA";
                    break;
                case "RAMP_GA_LARGE":
                    this.type = "RAMP_GA_LARGE";
                    break;
                case "RAMP_GA_MEDIUM":
                    this.type = "RAMP_GA_MEDIUM";
                    break;
                case "RAMP_GA_SMALL":
                    this.type = "RAMP_GA_SMALL";
                    break;
                case "RAMP_MIL_CARGO":
                    this.type = "RAMP_MIL_CARGO";
                    break;
                case "RAMP_MIL_COMBAT":
                    this.type = "RAMP_MIL_COMBAT";
                    break;
                case "VEHICLE":
                    this.type = "VEHICLE";
                    break;
                default:
                    throw new IllegalArgumentException("Type: Invalid type");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setName(String input) throws IllegalArgumentException {

        if (Utility.checkDuplicString(name, "Name")) {
            name = input.replaceAll("^\"|\"$", "");
        }

        /*
         String typeStr = input.replaceAll("^\"|\"$", "");

         switch (typeStr) {
         case "PARKING":
         name = "PARKING";
         break;
         case "DOCK":
         name = "DOCK";
         break;
         case "GATE":
         name = "GATE";
         break;
         case "GATE_A":
         name = "GATE_A";
         break;
         case "GATE_S":
         name = "GATE_S";
         case "GATE_Z":
         name = "GATE_Z";
         break;
         case "NONE":
         name = "NONE";
         break;
         case "N_PARKING":
         name = "N_PARKING";
         break;
         case "NE_PARKING":
         name = "NE_PARKING";
         break;
         case "NW_PARKING":
         name = "NW_PARKING";
         break;
         case "SE_PARKING":
         name = "SE_PARKING";
         break;
         case "S_PARKING":
         name = "S_PARKING";
         break;
         case "SW_PARKING":
         name = "SW_PARKING";
         break;
         case "W_PARKING":
         name = "W_PARKING";
         break;
         case "E_PARKING":
         name = "E_PARKING";
         break;
         default:
         throw new IllegalArgumentException("Name: Invalid Name");
         }
         */
    }

    /**
     *
     * @param input
     */
    public void setAirlineCodes(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(airlineCodes, "AirlineCodes")) {
            airlineCodes = input.replaceAll("^\"|\"$", "");
        }
    }

    /**
     *
     * @param input
     */
    public void setPushBack(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(pushBack, "PushBack")) {
            String typeStr = input.replaceAll("^\"|\"$", "");

            switch (typeStr) {
                case "NONE":
                    pushBack = "NONE";
                    break;
                case "BOTH":
                    pushBack = "BOTH";
                    break;
                case "LEFT":
                    pushBack = "LEFT";
                    break;
                case "RIGHT":
                    pushBack = "RIGHT";
                    break;
                default:
                    throw new IllegalArgumentException("Pushback: Invalid Pushback");
            }
        }
    }

    public String getType() {
        return type;
    }

    public int getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public String getAirlineCodes() {
        return airlineCodes;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public String getRadius() {
        return radius;
    }
    
    

}
