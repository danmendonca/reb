package elem;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Locale;
import java.util.Scanner;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class Utility {

    //Generate SDL file
    /**
     * 
     * @param outputFile
     * @param airportList 
     */
    public static void generateSDLfile(File outputFile, ArrayList<Airport> airportList) {

        try {

            //Factory and Builder
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            //scenario
            Document doc = docBuilder.newDocument();
            Element scenarioElement = doc.createElement("scenario");
            scenarioElement.setAttribute("xmlns", "dcs:scenario");
            scenarioElement.setAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
            scenarioElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            doc.appendChild(scenarioElement);

            //bases
            Element basesElement = doc.createElement("bases");

            //TODO: Process all airports
            int pos = 1;
            for (Airport ap : airportList) {
                Element baseOp = ap.generateSDLairport(doc, pos);
                basesElement.appendChild(baseOp);
                pos++;
            }
            
            scenarioElement.appendChild(basesElement);
            scenarioElement.appendChild(Utility.createElemTextInside(doc, "controllers", " "));
            scenarioElement.appendChild(Utility.createElemTextInside(doc, "agentTypes", " "));
            scenarioElement.appendChild(Utility.createElemTextInside(doc, "noFlyAreas", " "));

            //write to xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            //Indent xml output
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.VERSION, "1.0");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(outputFile);

            //Output to console for testing
            //StreamResult result = new StreamResult(System.out);

            transformer.transform(source, result);

            System.out.println(System.lineSeparator() + outputFile.getName() + " criado com sucesso.");

        } catch (Exception e) {
            System.out.println("Utility - " + e.getMessage());
        }

    }
    
    //Utilities for SDL generation
    /**
     * 
     * @param doc
     * @param elemName
     * @param text
     * @return 
     */
    public static Element createElemTextInside(Document doc,String elemName, String text){
        Element elem = doc.createElement(elemName);
        elem.appendChild(doc.createTextNode(text));
        return elem;
    }

    /**
     *
     */
    private Utility() {
        throw new AssertionError();
    }

    //Check duplicated attributes ==================================
    /**
     *
     * @param attr
     * @param nameAttr
     * @return
     * @throws IllegalArgumentException
     */
    static public boolean checkDuplicInt(int attr, String nameAttr) throws IllegalArgumentException {
        if (attr != Integer.MAX_VALUE) {
            throw new IllegalArgumentException(nameAttr + " - Duplicated Attribute");
        }
        return true;
    }

    /**
     *
     * @param attr
     * @param nameAttr
     * @return
     * @throws IllegalArgumentException
     */
    static public boolean checkDuplicDouble(double attr, String nameAttr) throws IllegalArgumentException {
        if (!Double.isNaN(attr)) {
            throw new IllegalArgumentException(nameAttr + " - Duplicated Attribute");
        }
        return true;
    }

    /**
     *
     * @param attr
     * @param nameAttr
     * @return
     * @throws IllegalArgumentException
     */
    static public boolean checkDuplicString(String attr, String nameAttr) throws IllegalArgumentException {
        if ((attr != null) && !attr.isEmpty()) {
            throw new IllegalArgumentException(nameAttr + " - Duplicated Attribute");
        }
        return true;
    }

    // Print methods =======================================
    /**
     *
     * @param n
     * @param i
     */
    static public void printInt(String n, int i) {
        if (i != Integer.MAX_VALUE) {
            System.out.println(n + " = " + i);
        }
    }

    /**
     *
     * @param n
     * @param d
     */
    static public void printDouble(String n, double d) {
        if (!Double.isNaN(d)) {
            System.out.println(n + " = " + d);
        }
    }

    /**
     *
     * @param n
     * @param v
     */
    static public void printString(String n, String v) {
        if ((v != null) && !v.isEmpty()) {
            System.out.println(n + " = " + v);
        }
    }

    //Check mandatory attributes ==================================
    /**
     *
     * @param n
     * @param i
     * @throws IllegalArgumentException
     */
    static public void checkMandatoryInt(String n, int i) throws IllegalArgumentException {
        if (i == Integer.MAX_VALUE) {
            throw new IllegalArgumentException(n + " - Missing mandatory attribute");
        }
    }

    /**
     *
     * @param n
     * @param d
     * @throws IllegalArgumentException
     */
    static public void checkMandatoryDouble(String n, double d) throws IllegalArgumentException {
        if (Double.isNaN(d)) {
            throw new IllegalArgumentException(n + " - Missing mandatory attribute");
        }
    }

    /**
     *
     * @param n
     * @param s
     * @throws IllegalArgumentException
     */
    static public void checkMandatoryString(String n, String s) throws IllegalArgumentException {
        if ((s == null) || s.isEmpty()) {
            throw new IllegalArgumentException(n + " - Missing mandatory attribute");
        }
    }

    // SET Generic Methods =====================================
    /**
     *
     * @param input
     * @param nameAttr
     * @param maxLength
     * @return
     * @throws IllegalArgumentException
     */
    static public String setString(String input, String nameAttr, int maxLength) throws IllegalArgumentException {
        String temp = input.replaceAll("^\"|\"$", "");

        if (temp.length() > maxLength) {
            throw new IllegalArgumentException(nameAttr + ": characters max = " + maxLength);
        }
        return temp;
    }

    /**
     *
     * @param input
     * @param nameAttr
     * @param minLim
     * @param maxLim
     * @return
     * @throws IllegalArgumentException
     * @throws NumberFormatException
     */
    static public double setDouble(String input, String nameAttr, double minLim, double maxLim) throws IllegalArgumentException, NumberFormatException {
        String temp = input.replaceAll("^\"|\"$", "");
        double tempDouble = Double.parseDouble(temp);

        if ((tempDouble < minLim) || (tempDouble > maxLim)) {
            throw new IllegalArgumentException(nameAttr + ": " + minLim + " to " + maxLim);
        }
        return tempDouble;
    }

    /**
     *
     * @param input
     * @param nameAttr
     * @param minLim
     * @param maxLim
     * @return
     * @throws IllegalArgumentException
     * @throws NumberFormatException
     */
    static public int setInt(String input, String nameAttr, int minLim, int maxLim) throws IllegalArgumentException, NumberFormatException {
        String temp = input.replaceAll("^\"|\"$", "");
        int tempInt = Integer.parseInt(temp);

        if ((tempInt < minLim) || (tempInt > maxLim)) {
            throw new IllegalArgumentException(nameAttr + ": " + minLim + " to " + maxLim);
        }
        return tempInt;
    }

    static public String setBoolean(String input, String nameAttr) throws IllegalArgumentException {
        String temp = input.replaceAll("^\"|\"$", "");

        if (temp.equals("TRUE") || temp.equals("FALSE")) {
            return temp;
        }

        throw new IllegalArgumentException(nameAttr + ": Must be TRUE or FALSE");
    }

    /**
     *
     * @param input
     * @param nameAttr
     * @return
     * @throws IllegalArgumentException
     * @throws NumberFormatException
     */
    static public String setMeterFeet(String input, String nameAttr) throws IllegalArgumentException, NumberFormatException {
        String temp = input.replaceAll("^\"|\"$", "");

        String lastLetter = Character.toString(temp.charAt(temp.length() - 1));

        if (!lastLetter.equals("F") && !lastLetter.equals("M") && !isNumeric(lastLetter)) {
            throw new IllegalArgumentException(nameAttr + ": may be suffixed by 'M' or 'F' to designate meters or feet.");
        }

        String decimalPart = temp.substring(0, temp.length() - 1);
        Double.parseDouble(decimalPart);
        return temp;
    }

    /**
     *
     * @param str
     * @return
     */
    static public boolean isNumeric(String str) {
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    /**
     *
     * @param input
     * @param nameAttr
     * @return
     * @throws IllegalArgumentException
     * @throws NumberFormatException
     */
    static public String setNauticalMeterFeet(String input, String nameAttr) throws IllegalArgumentException, NumberFormatException {
        String temp = input.replaceAll("^\"|\"$", "");

        String lastLetter = Character.toString(temp.charAt(temp.length() - 1));

        if (!lastLetter.equals("F") && !lastLetter.equals("M") && !lastLetter.equals("N") && !isNumeric(lastLetter)) {
            throw new IllegalArgumentException(nameAttr + ": F, M, N suffix.");
        }

        String decimalPart = temp.substring(0, temp.length() - 1);
        Double.parseDouble(decimalPart);
        return temp;
    }

    /**
     *
     * @param input
     * @param limit
     * @return
     */
    static public String setDegMinSec(String input, double limit) {
        String temp = input.replaceAll("^\"|\"$", "");

        int degrees;
        double minSec;

        String direction = Character.toString(temp.charAt(0));

        if (!direction.equals("N") && !direction.equals("S") && !direction.equals("W") && !direction.equals("E")) {
            return null;
        }

        String temp2 = temp.substring(1, temp.length() - 1);
        Scanner s = new Scanner(temp2);

        if (!s.hasNextInt()) {
            return null;
        }
        degrees = s.nextInt();

        if (degrees < 0 || degrees > limit) {
            return null;
        }

        s.useLocale(Locale.US);
        if (!s.hasNextDouble()) {
            return null;
        }

        minSec = s.nextDouble();

        if (s.hasNext()) {
            return null;
        }

        return temp;
    }
    
    
    public static String[] unitAndNumber(String str){
        String values[] = new String[2];
        
        String unit = ""+str.charAt(str.length()-1);
        
        switch(unit.toUpperCase()){
            case "M":
                values[0] = "Meter";
                break;
            case "CM":
                values[0] = "Centimeter";
                break;
            case "KM":
                values[0]="Kilometer";
                break;
            case "FT":
                values[0] = "Foot";
                break;
            case "IN":
                values[0]="Inch";
                break;
            case "MI":
                values[0] = "Mile (Statue)";
                break;
            case "NM":
                values[0]= "Nautical Mile";
                break;
            default:
                values[0] = unit;
                break;
        }
        
        values[1] = str.substring(0, str.length()-1);
        
        
        
        return values;
    }

}
