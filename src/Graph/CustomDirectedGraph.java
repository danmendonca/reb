/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Graph;

import java.util.ArrayList;
import java.util.Set;
import org.jgrapht.graph.DefaultDirectedGraph;

/**
 *
 * @author Daniel
 */
public class CustomDirectedGraph extends DefaultDirectedGraph {

    public CustomDirectedGraph() {
        super(RelationshipEdge.class);
    }

    void setRunwayStarts() {

    }

    void setTaxiStarts() {

    }

    public ArrayList<RelationshipEdge> getAllRunwayEdges() {

        Set<RelationshipEdge> eds = super.edgeSet();

        ArrayList<RelationshipEdge> runEdges = new ArrayList<>();

        for (RelationshipEdge re : eds) {
            if (re.getLabel().equalsIgnoreCase("RUNWAY")) {
                runEdges.add(re);
            }
        }

        return runEdges;
    }

    public ArrayList<RelationshipEdge> getAllTaxiEdges() {

        Set<RelationshipEdge> eds = super.edgeSet();

        ArrayList<RelationshipEdge> taxiEdges = new ArrayList<>();

        for (RelationshipEdge re : eds) {
            if (re.getLabel().equalsIgnoreCase("TAXI")) {
                taxiEdges.add(re);
            }
        }

        return taxiEdges;
    }

    public ArrayList<RelationshipEdge> getAllTaxiEdges(String identifier) {

        ArrayList<RelationshipEdge> taxiEdgesId = new ArrayList<>();
        Set<RelationshipEdge> eds = super.edgeSet();

        for (RelationshipEdge re : eds) {
            if (re.getLabel().equalsIgnoreCase("TAXI")) {
                if (re.getIdentifier().equalsIgnoreCase(identifier)) {
                    return taxiEdgesId;
                }
            }
        }

        return taxiEdgesId;
    }

    public ArrayList<RelationshipEdge> getAllRunwayEdges(String identifier) {

        Set<RelationshipEdge> eds = super.edgeSet();

        ArrayList<RelationshipEdge> runEdges = new ArrayList<>();

        for (RelationshipEdge re : eds) {
            if (re.getLabel().equalsIgnoreCase("RUNWAY")) {
                if (re.getIdentifier().equalsIgnoreCase(identifier)) {
                    runEdges.add(re);
                }
            }
        }

        return runEdges;
    }

    public RelationshipEdge getRunwayOfIdFromVertex(int vertexId, String edgeNumber){
        
        return null;
    }
            
}
