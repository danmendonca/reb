package Graph;

import org.jgrapht.graph.DefaultEdge;

public class RelationshipEdge<V> extends DefaultEdge {

    private V v1;
    private V v2;
    private String label;
    boolean isStart;
    private String identifier;
    
    private int name;
    private String number;
    
    private String surface;
    private String width;

    public RelationshipEdge(V v1, V v2, String label) {
        this.v1 = v1;
        this.v2 = v2;
        this.label = label;
        isStart = false;
    }
    
    public RelationshipEdge(V v1, V v2, String label, int name, String number) {
        this.v1 = v1;
        this.v2 = v2;
        
        this.label = label;
        this.name = name;
        this.number = number;
        
    }

    public int getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

    public String getSurface() {
        return surface;
    }

    public void setSurface(String surface) {
        this.surface = surface;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }
    
    
    
    
    
    

    public V getV1() {
        return v1;
    }

    public V getV2() {
        return v2;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isIsStart() {
        return isStart;
    }

    public void setIsStart(boolean isStart) {
        this.isStart = isStart;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String toString() {
        return label;
    }
}
