grammar TaxiWayPoint;

@members {          
    boolean tagMode = false;
}

//=====================================
//LEXER
//=====================================
TAG_START_OPEN : '<' {tagMode = true;};
TAG_END_OPEN : '</' {tagMode = true;};
TAG_CLOSE : {tagMode}? '>' {tagMode = false;};
TAG_EMPTY_CLOSE : {tagMode}? '/>' {tagMode = false;};

//Attribute values
ATTR_EQ : {tagMode}? '=';
ATTR_DQ : {tagMode}? '"';
ATTR_QU : {tagMode}? '\'';

WS: [ \t\r\n]+ -> skip ;

//INT
ATTR_VALUE_INT : { tagMode }? ( ATTR_DQ ('-')?(DIGIT)* ATTR_DQ | ATTR_QU ('-')?(DIGIT)* ATTR_QU);

//REAL
ATTR_VALUE_REAL : { tagMode }? ( ATTR_DQ ('-')?(DIGIT)*'.'(DIGIT)+ ATTR_DQ | ATTR_QU ('-')?(DIGIT)*'.'(DIGIT)+ ATTR_QU) 
| ( ATTR_DQ ('-')?(DIGIT)+'.'(DIGIT)* ATTR_DQ | ATTR_QU ('-')?(DIGIT)+'.'(DIGIT)* ATTR_QU) ;

//STR
ATTR_VALUE_STR : { tagMode }? ( ATTR_DQ (NAMECHAR)* ATTR_DQ | ATTR_QU (NAMECHAR)* ATTR_QU);

//~ -> this means anything but the character that follows
PCDATA : {!tagMode}? (~'<')+ ;

NAMECHAR : LETTER | DIGIT | '.' | '-' | '_' | ':' ;
DIGIT : [0-9] ;
LETTER : [a-z] | [A-Z] ;


//COMMON ATTRIBUTES
LAT : 'lat';
LON : 'lon';
ALT : 'alt';
TYPE : 'type';

//=====================================
//PARSER
//=====================================
//XML Doc
document : xmlElem EOF;

//XML Element
xmlElem : airportStart
          taxiwaypointElem+
          serviceElem?
          towerElem?
          airportEnd;

/*
//INT
attributeValueInt : ATTR_VALUE_INT {
                                try{
                                    String str = new String($ATTR_VALUE_INT.text);
                                    int result = Integer.parseInt(str.replaceAll("^\"|^\'|\'$|\"$", ""));
                                } catch(NumberFormatException e){
                                    System.err.println($ATTR_VALUE_INT.line + ":" + $ATTR_VALUE_INT.pos + " - " + "Error: Attribute value should be a integer.");
                                }
};
*/

//=====================================
//Airport
//=====================================
AIRPORT_TAG : 'Airport';

//Airport - Attributes
AIRPORT_IDENT : 'ident';
AIRPORT_TEST_RADIUS : 'airportTestRadius';
AIRPORT_TRAFFIC_SCALAR : 'trafficScalar';
AIRPORT_REGION_OP : 'region';
AIRPORT_COUNTRY_OP : 'country';
AIRPORT_STATE_OP : 'state';
AIRPORT_CITY_OP : 'city';
AIRPORT_NAME_OP : 'name';
AIRPORT_MAGVAR_OP : 'magvar';

//Airport - Rules
airportStart : TAG_START_OPEN AIRPORT_TAG
             ((airportAttrReal ATTR_EQ ATTR_VALUE_REAL) | (airportAttrStr ATTR_EQ ATTR_VALUE_STR))*
             TAG_CLOSE; 

airportEnd : TAG_END_OPEN AIRPORT_TAG TAG_CLOSE;

airportAttrReal : LAT
                | LON
                | AIRPORT_MAGVAR_OP
                | AIRPORT_TRAFFIC_SCALAR
                | AIRPORT_TEST_RADIUS;

airportAttrStr : AIRPORT_REGION_OP
               | AIRPORT_COUNTRY_OP
               | AIRPORT_STATE_OP
               | AIRPORT_CITY_OP
               | AIRPORT_NAME_OP
               | ALT
               | AIRPORT_IDENT
               | AIRPORT_TEST_RADIUS;

//=====================================
//Services
//=====================================
SERVICES_TAG : 'Services';

//Service - Rules
//There are no attributes that can be applied to the Services element.
serviceElem : serviceStart fuelElem? serviceEnd;
serviceStart : TAG_START_OPEN SERVICES_TAG TAG_CLOSE; 
serviceEnd : TAG_END_OPEN SERVICES_TAG TAG_CLOSE;
     
//=====================================
//Fuel
//=====================================
FUEL_TAG : 'Fuel';

//Fuel - Attributes
FUEL_AVAILABILITY : 'availability';

//Fuel - Rules
fuelElem : TAG_START_OPEN FUEL_TAG 
         ((fuelAttrStr ATTR_EQ ATTR_VALUE_STR) | (TYPE ATTR_EQ ATTR_VALUE_INT))*
         TAG_EMPTY_CLOSE;

fuelAttrStr : TYPE | FUEL_AVAILABILITY;

//=====================================
//Tower
//=====================================
TOWER_TAG : 'Tower';

//Tower - Rules
towerElem : TAG_START_OPEN TOWER_TAG
          ((towerAttrStr ATTR_EQ ATTR_VALUE_STR) | (towerAttrReal ATTR_EQ ATTR_VALUE_REAL))*
          TAG_EMPTY_CLOSE;

towerAttrReal : LAT | LON;
towerAttrStr : ALT;
//TaxiWayPoint Tokens==========================

//taxiwaypoint tag
TAXIWAYPOINT_TAG : 'TaxiwayPoint';
  
//taxiway attributes
TAXIWAYPOINT_INDEX: 'index';

TAXIWAYPOINTORIENTATION: 'orientation';

TAXIWAYPOINT_BIASX:'biasX';


TAXIWAYPOINT_BIASZ:'biasZ';


//TaxiWayPoint rules
taxiwayPointAtts: TYPE | TAXIWAYPOINTORIENTATION | TAXIWAYPOINT_INDEX | LAT | LON | TAXIWAYPOINT_BIASX | TAXIWAYPOINT_BIASZ;

taxiwayPointAttStr:
                   TYPE|
                   TAXIWAYPOINTORIENTATION;

taxiwayPointAttReal: TAXIWAYPOINT_BIASX |
                     TAXIWAYPOINT_BIASZ |
                     LON |
                     LAT;
taxiwayPointAttInt: TAXIWAYPOINT_INDEX; 

taxiwaypointElem : TAG_START_OPEN TAXIWAYPOINT_TAG 
                   (taxiwayPointAttInt ATTR_EQ ATTR_VALUE_INT |
                   taxiwayPointAttReal ATTR_EQ ATTR_VALUE_REAL |
                   taxiwayPointAttStr ATTR_EQ ATTR_VALUE_STR)* 
                   TAG_EMPTY_CLOSE;


//TaxiwayPoint possible regexs =================================================

//TAXIWAYPOINT_BIASZ_ATT: FLOAT_ATT;
//TAXIWAYPOINT_BIASX_ATT: FLOAT_ATT;
//TAXIWAYPOINT_LON_ATT: FLOAT_ATT;
//TAXIWAYPOINT_LAT_ATT: FLOAT_ATT;
//TAXIWAYPOINT_ORIENTATION_ATT: 'FORWARD' | 'REVERSE';
//TAXIWAYPOINT_TYPE_ATT : 'NORMAL' |
//'HOLD_SHORT' |
//'ILS_HOLD_SHORT' |
//'HOLD_SHORT_NO_DRAW' |
//'ILS_HOLD_SHORT_NO_DRAW' ;
//TAXYWAYPOINT_INDEX_ATT: POS_INT_ATT;

//POS_INT_ATT: [1-9]DIGIT* | '0';
//FLOAT_ATT: '-'?DIGIT+('.'DIGIT+)?;
//QUOTE: '"';




//TaxiwayPoint possible rules ==================================================

//taxiwaypointAtts : twpTypeRule | twpIndexRule | twpOrientationRule|
//                         twpLonRule | twpLatRule | twpBiasXRule | twpBiasZRule;

//twpTypeRule:TAXIWAYPOINT_TYPE ATTR_EQ QUOTE TAXIWAYPOINT_TYPE_ATT QUOTE;
//twpIndexRule: TAXIWAYPOINT_INDEX ATTR_EQ QUOTE TAXYWAYPOINT_INDEX_ATT QUOTE;
//twpOrientationRule: TAXIWAYPOINT_ORIENTATION ATTR_EQ QUOTE TAXIWAYPOINT_ORIENTATION_ATT QUOTE;
//twpLatRule: TAXIWAYPOINT_LAT ATTR_EQ QUOTE TAXIWAYPOINT_LAT_ATT QUOTE;
//twpLonRule:  TAXIWAYPOINT_LON ATTR_EQ QUOTE TAXIWAYPOINT_LON_ATT QUOTE;
//twpBiasXRule: TAXIWAYPOINT_BIASX ATTR_EQ QUOTE TAXIWAYPOINT_BIASX_ATT QUOTE;
//twpBiasZRule: TAXIWAYPOINT_BIASZ ATTR_EQ QUOTE TAXIWAYPOINT_BIASZ_ATT QUOTE;