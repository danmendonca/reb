/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

grammar Runway;


@members {          
    boolean tagMode = false;
}

//=====================================
//LEXER
//=====================================
TAG_START_OPEN : '<' {tagMode = true;};
TAG_END_OPEN : '</' {tagMode = true;};
TAG_CLOSE : {tagMode}? '>' {tagMode = false;};
TAG_EMPTY_CLOSE : {tagMode}? '/>' {tagMode = false;};

//Attribute values
ATTR_EQ : {tagMode}? '=';
ATTR_DQ : {tagMode}? '"';
ATTR_QU : {tagMode}? '\'';

WS: [ \t\r\n]+ -> skip ;

//INT
//ATTR_VALUE_INT_UNIT : { tagMode }? ( ATTR_DQ ('-')?(DIGIT)* ('M'|'F') ATTR_DQ | ATTR_QU ('-')?(DIGIT)* ('M'|'F') ATTR_QU);
ATTR_VALUE_INT : { tagMode }? ( ATTR_DQ ('-')?(DIGIT)* ATTR_DQ | ATTR_QU ('-')?(DIGIT)* ATTR_QU);

//REAL
ATTR_VALUE_REAL : { tagMode }? ((ATTR_DQ '-'? DIGIT+ '.' DIGIT+ ATTR_DQ) | (ATTR_QU '-'? DIGIT+ '.' DIGIT+ ATTR_QU));

//STR
ATTR_VALUE_STR : { tagMode }? ( ATTR_DQ (~'"')* ATTR_DQ | ATTR_QU (~'\'')* ATTR_QU);

//~ -> this means anything but the character that follows
PCDATA : {!tagMode}? (~'<')+ ;

NAMECHAR : LETTER | DIGIT | '.' | '-' | '_' | ':';
DIGIT : [0-9] ;
LETTER : [a-z] | [A-Z] ;


//COMMON ATTRIBUTES
LAT : 'lat';
LON : 'lon';
ALT : 'alt';
TYPE : 'type';
INDEX : 'index';
BIASX:'biasX';
BIASZ:'biasZ';
NAME : 'name';
NUMBER: 'number';

//=====================================
//PARSER
//=====================================
//XML Doc
document : xmlElem EOF;

//XML Element
xmlElem : airportStart
          //taxiwaypointElem+
          //taxiwayparkingElem+
          //taxiwayPathElem+
          //serviceElem?
          //towerElem?
          runwayElem?
          airportEnd;

/*
//INT
attributeValueInt : ATTR_VALUE_INT {
                                try{
                                    String str = new String($ATTR_VALUE_INT.text);
                                    int result = Integer.parseInt(str.replaceAll("^\"|^\'|\'$|\"$", ""));
                                } catch(NumberFormatException e){
                                    System.err.println($ATTR_VALUE_INT.line + ":" + $ATTR_VALUE_INT.pos + " - " + "Error: Attribute value should be a integer.");
                                }
};
*/

//=====================================
//Airport
//=====================================
AIRPORT_TAG : 'Airport';

//Airport - Attributes
IDENT : 'ident';
AIRPORT_TEST_RADIUS : 'airportTestRadius';
AIRPORT_TRAFFIC_SCALAR : 'trafficScalar';
AIRPORT_REGION_OP : 'region';
AIRPORT_COUNTRY_OP : 'country';
AIRPORT_STATE_OP : 'state';
AIRPORT_CITY_OP : 'city';
MAGVAR : 'magvar';

//Airport - Rules
airportStart : TAG_START_OPEN AIRPORT_TAG
             ((airportAttrReal ATTR_EQ ATTR_VALUE_REAL) | (airportAttrStr ATTR_EQ ATTR_VALUE_STR))*
             TAG_CLOSE; 

airportEnd : TAG_END_OPEN AIRPORT_TAG TAG_CLOSE;

airportAttrReal : LAT
                | LON
                | MAGVAR
                | AIRPORT_TRAFFIC_SCALAR
                | AIRPORT_TEST_RADIUS;

airportAttrStr : AIRPORT_REGION_OP
               | AIRPORT_COUNTRY_OP
               | AIRPORT_STATE_OP
               | AIRPORT_CITY_OP
               | NAME
               | ALT
               | IDENT
               | AIRPORT_TEST_RADIUS;


//=====================================
//Runway
//=====================================

RUNWAY_TAG: 'Runway';

SURFACE: 'surface';
HEADING: 'heading';
WIDTH: 'width';
LENGTH: 'length';
DESIGNATOR: 'designator';
PRIMARY_DESIGNATOR: 'primaryDesignator';
SECONDARY_DESIGNATOR: 'secondaryDesignator';
PATTERN_ALTITUDE: 'patternAltitude';
PRIMARY_TAKE_OFF: 'primaryTakeoff';
PRIMARY_LANDING: 'primaryLanding';
PRIMARY_PATTERN: 'primaryPattern';
SECONDARY_TAKE_OFF: 'secondaryTakeoff';
SECONDARY_LANDING: 'secondaryLanding';
SECONDARY_PATTERN: 'secondaryPattern';
PRIMARY_MARKING_BIAS: 'primaryMarkingBias';
SECONDARY_MARKING_BIAS: 'secondaryMarkingBias';


runwayAttString: 
                 ALT |
                 SURFACE |
                 LENGTH |
                 WIDTH |
                 NUMBER |
                 DESIGNATOR |
                 PRIMARY_DESIGNATOR |
                 SECONDARY_DESIGNATOR |
                 PATTERN_ALTITUDE |                
                 PRIMARY_TAKE_OFF |
                 PRIMARY_LANDING |
                 PRIMARY_PATTERN |
                 SECONDARY_TAKE_OFF |
                 SECONDARY_LANDING |
                 SECONDARY_PATTERN |
                 PRIMARY_MARKING_BIAS |
                 SECONDARY_MARKING_BIAS
;
runwayAttReal: LAT |
               LON |
               HEADING |
               ALT |
               NUMBER |
               DESIGNATOR |
               PRIMARY_DESIGNATOR |
               SECONDARY_DESIGNATOR |
               PATTERN_ALTITUDE
;
runwayAttInt: NUMBER
;

runwayNestedElem: markingsElem
                | lightsElem
                | offsetThresholdElem
                | blastPadElem
                | overrunElem
                | approachLightsElem
                | vasiElem
                | ilsElem
                | runwayStartElem
                ;

runwayElemStart: TAG_START_OPEN RUNWAY_TAG 
            (runwayAttString ATTR_EQ ATTR_VALUE_STR|
            runwayAttReal ATTR_EQ ATTR_VALUE_REAL |
            runwayAttInt ATTR_EQ ATTR_VALUE_INT)*
            TAG_CLOSE;

runwayElemClose: TAG_END_OPEN RUNWAY_TAG TAG_CLOSE;

runwayElem: runwayElemStart runwayNestedElem* runwayElemClose;


//=====================================
//Markings
//=====================================
MARKINGS_TAG : 'Markings';

ALTERNATETHRESHOLD: 'alternateThreshold';
ALTERNATETOUCHDOWN: 'alternateTouchdown';
ALTERNATEFIXEDDISTANCE: 'alternateFixedDistance';
ALTERNATEPRECISION: 'alternatePrecision';
LEADINGZEROSIDENT: 'leadingZeroIdent';
NOTHRESHOLDENDARROWS: 'noThresholdEndArrows';
EDGES: 'edges';
THRESHOLD: 'threshold';
FIXED_DISTANCE: 'fixedDistance';
FIXED:'fixed';
TOUCHDOWN: 'touchdown';
DASHES: 'dashes';
PRECISION: 'precision';
EDGEPAVEMENT:'edgePavement';
SINGLEEND:'singleEnd';
PRIMARYCLOSE:'primaryClosed';
SECONDARYCLOSED:'secondaryClosed';
PRIMARYSTOL: 'primaryStol';
SECONDARYSTOL:'secondaryStol';

markingsAttsBool: 
                  ALTERNATETHRESHOLD |
                  ALTERNATETOUCHDOWN |
                  ALTERNATEFIXEDDISTANCE |
                  ALTERNATEPRECISION |
                  LEADINGZEROSIDENT |
                  NOTHRESHOLDENDARROWS |
                  EDGES |
                  THRESHOLD |
                  FIXED |
                  TOUCHDOWN |
                  DASHES |
                  IDENT |
                  PRECISION |
                  EDGEPAVEMENT |
                  SINGLEEND |
                  PRIMARYCLOSE |
                  SECONDARYCLOSED |
                  PRIMARYSTOL |
                  SECONDARYSTOL |
                  FIXED_DISTANCE
                ;
markingsElem : TAG_START_OPEN MARKINGS_TAG
               (markingsAttsBool ATTR_EQ ATTR_VALUE_STR)+
               TAG_EMPTY_CLOSE;

//=====================================
//Lights
//=====================================
LIGHTS_TAG: 'Lights';

CENTER:'center';
EDGE:'edge';
CENTERRED:'centerRed';

lightsAttStr: CENTER 
            | EDGE
            | CENTERRED;

lightsElem : TAG_START_OPEN LIGHTS_TAG
               (lightsAttStr ATTR_EQ ATTR_VALUE_STR)+
               TAG_EMPTY_CLOSE;

//=====================================
//OffsetThreshold
//=====================================
OFFSETTHRESHOLD_TAG: 'OffsetThreshold';

END: 'end';

offsetThresholdAttStr: 
                     END |
                     WIDTH |
                     LENGTH |
                     SURFACE
;
offsetThresholdElem : TAG_START_OPEN OFFSETTHRESHOLD_TAG
                      (offsetThresholdAttStr ATTR_EQ ATTR_VALUE_STR)*
                      TAG_EMPTY_CLOSE
;

//=====================================
//BlastPad
//=====================================
BLASTPAD_TAG: 'BlastPad';


blastPadAttStr:
               END |
               LENGTH |
               WIDTH |
               SURFACE
              ;
blastPadElem: TAG_START_OPEN BLASTPAD_TAG
              (blastPadAttStr ATTR_EQ ATTR_VALUE_STR)+
              TAG_EMPTY_CLOSE;

//=====================================
//Overrun
//=====================================
OVERRUN_TAG : 'Overrun';

overrunAttStr:
               END |
               LENGTH |
               WIDTH |
               SURFACE
              ;
overrunElem: TAG_START_OPEN OVERRUN_TAG
              (overrunAttStr ATTR_EQ ATTR_VALUE_STR)+
              TAG_EMPTY_CLOSE;


//=====================================
//ApproachLights
//=====================================
APPROACHLIGHTS_TAG: 'ApproachLights';

SYSTEM: 'system';
REIL : 'reil';
ENDLIGHTS: 'endLights';
STROBES: 'strobes';

approachLightsAttStr: END 
                    | SYSTEM
                    | REIL 
                    | TOUCHDOWN 
                    | ENDLIGHTS
                    ;

approachLightsAttPosInt: STROBES;

approachLightsElem: TAG_START_OPEN APPROACHLIGHTS_TAG
                    (approachLightsAttPosInt ATTR_EQ ATTR_VALUE_INT |
                    approachLightsAttStr ATTR_EQ ATTR_VALUE_STR)+
                    TAG_EMPTY_CLOSE;

//=====================================
//Vasi
//=====================================
VASI_TAG: 'Vasi';

SIDE:'side';
SPACING:'spacing';
PITCH:'pitch';

vasiAttStr:
          END |
          TYPE |
          SIDE |
          BIASX |
          BIASZ |
          SPACING
          ;

vasiElem: TAG_START_OPEN VASI_TAG
          ( vasiAttStr ATTR_EQ ATTR_VALUE_STR
          | PITCH ATTR_EQ ATTR_VALUE_REAL
          )+
          TAG_EMPTY_CLOSE
        ;


//=====================================
//ILS
//=====================================
ILS_TAG : 'Ils';


FREQUENCY: 'frequency';
RANGE: 'range';
BACKCOURSE: 'backCourse';

ilsAttStr: NAME
         | END
         | IDENT
         | BACKCOURSE
         ;

ilsAttInt: RANGE
         ;

ilsAttReal:
           LAT |
           LON |
           ALT |
           HEADING |
           FREQUENCY |
           MAGVAR |
           WIDTH
          ;

ilsElemStart:
        TAG_START_OPEN ILS_TAG
        ( ilsAttInt ATTR_EQ ATTR_VALUE_INT
        | ilsAttStr ATTR_EQ ATTR_VALUE_STR
        | ilsAttReal ATTR_EQ ATTR_VALUE_REAL
        )+
        TAG_CLOSE
       ;

ilsElemClose:
             TAG_END_OPEN ILS_TAG TAG_CLOSE;


ilsElem: ilsElemStart(
         glideSlopeElem
         dmeElem
         visualModelElem
                     )* ilsElemClose
       ;


//=====================================
//GlideSlope
//=====================================
GLIDESLOPE_TAG : 'GlideSlope';

glideSlopeAttReal: LAT
                 | LON
                 | ALT
                 | PITCH
                 ;
glideSlopeAttInt: RANGE;

glideSlopeAttStr: LAT
                 | LON
                 | RANGE
                 | ALT
                ;

glideSlopeElem:   TAG_START_OPEN GLIDESLOPE_TAG
                  ( glideSlopeAttReal ATTR_EQ ATTR_VALUE_REAL
                  | glideSlopeAttInt ATTR_EQ ATTR_VALUE_INT 
                  | glideSlopeAttStr ATTR_EQ ATTR_VALUE_STR
                  )+
                  TAG_EMPTY_CLOSE;

//=====================================
//DME
//=====================================
DME_TAG : 'Dme';

dmeAttReal: LAT
                 | LON
                 | ALT
                 ;
dmeAttStr: LAT
                 | LON
                 | ALT
                 | RANGE
                 ;

dmeElem:   TAG_START_OPEN DME_TAG
                  ( dmeAttReal ATTR_EQ ATTR_VALUE_REAL
                  | RANGE ATTR_EQ ATTR_VALUE_INT 
                  | dmeAttStr ATTR_EQ ATTR_VALUE_STR)+
                  TAG_EMPTY_CLOSE;


//=====================================
//VisualModel
//=====================================
VISUALMODEL_TAG : 'VisualModel';

IMAGECOMPLEXITY: 'imageComplexity';

visualModelAttReal: HEADING
                  ;
visualModelAttStr: IMAGECOMPLEXITY
                 | NAME;
visualModelElemStart: TAG_START_OPEN VISUALMODEL_TAG
                  ( visualModelAttReal ATTR_EQ ATTR_VALUE_REAL
                  | visualModelAttStr ATTR_EQ ATTR_VALUE_STR ) +
                  TAG_CLOSE;

visualModelElemClose:
             TAG_END_OPEN VISUALMODEL_TAG TAG_CLOSE;

visualModelElem: visualModelElemStart biasXYZElem? visualModelElemClose;



//=====================================
//BiasXYZ
//=====================================
BiasXYZ_TAG : 'BiasXYZ';

BIASY: 'biasY';
biasXYZAttReal: BIASX
              | BIASY
              | BIASZ
;
biasXYZAttInt: BIASX
             | BIASY
             | BIASZ
;
                

biasXYZElem: TAG_START_OPEN BiasXYZ_TAG
                  ( biasXYZAttReal ATTR_EQ ATTR_VALUE_REAL
                  | biasXYZAttInt ATTR_EQ ATTR_VALUE_INT)+
                  TAG_EMPTY_CLOSE;

//=====================================
//RunwayStart
//=====================================
RUNWAYSTART_TAG : 'RunwayStart';

runwayStartAttReal: LAT
                  | LON
                  | ALT
                  | HEADING
                  ;
runwayStartAttStr: TYPE
                 | END
                 ;

runwayStartElem: TAG_START_OPEN RUNWAYSTART_TAG
                 ( runwayStartAttReal ATTR_EQ ATTR_VALUE_REAL
                 | runwayStartAttStr ATTR_EQ ATTR_VALUE_STR)+
                 TAG_EMPTY_CLOSE;