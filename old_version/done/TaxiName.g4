/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

grammar TaxiName;
@members {          
    boolean tagMode = false;
}

/*
<TaxiName
      index="0"     
      name="A"
/>
Attribute       Description                     Acceptable Values
index           Unique index of this name 	Valid integer range is 0 to 255.
name            Text name                       8 chars max (can be NULL)
*/

//=====================================
//TaxiName
//=====================================

TAXINAME_TAG : 'TaxiName';
INDEX : 'index';
NAME: 'name';


taxiNameElem: TAG_START_OPEN TAXINAME_TAG (INDEX ATTR_EQ ATTR_VALUE_INT | 
                                            NAME ATTR_EQ ATTR_VALUE_STR)+
            TAG_EMPTY_CLOSE;


//=====================================
//LEXER
//=====================================
TAG_START_OPEN : '<' {tagMode = true;};
TAG_END_OPEN : '</' {tagMode = true;};
TAG_CLOSE : {tagMode}? '>' {tagMode = false;};
TAG_EMPTY_CLOSE : {tagMode}? '/>' {tagMode = false;};

//Attribute values
ATTR_EQ : {tagMode}? '=';
ATTR_DQ : {tagMode}? '"';
ATTR_QU : {tagMode}? '\'';

WS: [ \t\r\n]+ -> skip ;

//INT
ATTR_VALUE_INT : { tagMode }? ( ATTR_DQ ('-')?(DIGIT)* ATTR_DQ | ATTR_QU ('-')?(DIGIT)* ATTR_QU);

//REAL
ATTR_VALUE_REAL : { tagMode }? ((ATTR_DQ '-'? DIGIT+ '.' DIGIT+ ATTR_DQ) | (ATTR_QU '-'? DIGIT+ '.' DIGIT+ ATTR_QU));

//STR
ATTR_VALUE_STR : { tagMode }? ( ATTR_DQ (~'"')* ATTR_DQ | ATTR_QU (~'\'')* ATTR_QU);

//~ -> this means anything but the character that follows
PCDATA : {!tagMode}? (~'<')+ ;

NAMECHAR : LETTER | DIGIT | '.' | '-' | '_' | ':';
DIGIT : [0-9] ;
LETTER : [a-z] | [A-Z] ;


//COMMON ATTRIBUTES
LAT : 'lat';
LON : 'lon';
ALT : 'alt';
TYPE : 'type';
BIASX:'biasX';
BIASZ:'biasZ';
NUMBER: 'number';

//=====================================
//PARSER
//=====================================
//XML Doc
document : xmlElem EOF;

//XML Element
xmlElem : airportStart
          //taxiwaypointElem+
          //taxiwayparkingElem+
          //taxiwayPathElem+
          //serviceElem?
          //towerElem?
          taxiNameElem
          airportEnd;

/*
//INT
attributeValueInt : ATTR_VALUE_INT {
                                try{
                                    String str = new String($ATTR_VALUE_INT.text);
                                    int result = Integer.parseInt(str.replaceAll("^\"|^\'|\'$|\"$", ""));
                                } catch(NumberFormatException e){
                                    System.err.println($ATTR_VALUE_INT.line + ":" + $ATTR_VALUE_INT.pos + " - " + "Error: Attribute value should be a integer.");
                                }
};
*/

//=====================================
//Airport
//=====================================
AIRPORT_TAG : 'Airport';

//Airport - Attributes
AIRPORT_IDENT : 'ident';
AIRPORT_TEST_RADIUS : 'airportTestRadius';
AIRPORT_TRAFFIC_SCALAR : 'trafficScalar';
AIRPORT_REGION_OP : 'region';
AIRPORT_COUNTRY_OP : 'country';
AIRPORT_STATE_OP : 'state';
AIRPORT_CITY_OP : 'city';
AIRPORT_MAGVAR_OP : 'magvar';

//Airport - Rules
airportStart : TAG_START_OPEN AIRPORT_TAG
             ((airportAttrReal ATTR_EQ ATTR_VALUE_REAL) | (airportAttrStr ATTR_EQ ATTR_VALUE_STR))*
             TAG_CLOSE; 

airportEnd : TAG_END_OPEN AIRPORT_TAG TAG_CLOSE;

airportAttrReal : LAT
                | LON
                | AIRPORT_MAGVAR_OP
                | AIRPORT_TRAFFIC_SCALAR
                | AIRPORT_TEST_RADIUS;

airportAttrStr : AIRPORT_REGION_OP
               | AIRPORT_COUNTRY_OP
               | AIRPORT_STATE_OP
               | AIRPORT_CITY_OP
               | NAME
               | ALT
               | AIRPORT_IDENT
               | AIRPORT_TEST_RADIUS;


