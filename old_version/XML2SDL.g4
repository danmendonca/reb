grammar XML2SDL;

//=====================================
//LEXER
//=====================================

TAG_START_OPEN : '<';
TAG_END_OPEN : '</';
TAG_CLOSE : '>';
TAG_EMPTY_CLOSE : '/>';

//Attribute values
ATTR_EQ : '=';
ATTR_DQ : '"';
ATTR_QU : '\'';

//TAGS
AIRPORT : 'Airport';
TAXIWAYPOINT : 'TaxiwayPoint';
SERVICES : 'Services';
FUEL : 'Fuel';
TOWER : 'Tower';
TAXIWAYPARKING : 'TaxiwayParking';
ROUTE : 'Route';
PREVIOUS : 'Previous';
NEXT : 'Next';
TAXIWAYPATH: 'TaxiwayPath';
RUNWAY_ALIAS: 'RunwayAlias';
COM: 'Com';
TAXINAME : 'TaxiName';
RUNWAY: 'Runway';
MARKINGS : 'Markings';
LIGHTS: 'Lights';
OFFSETTHRESHOLD: 'OffsetThreshold';
BLASTPAD: 'BlastPad';
OVERRUN : 'Overrun';
APPROACHLIGHTS : 'ApproachLights';
VASI : 'Vasi';
ILS : 'Ils';
GLIDESLOPE : 'GlideSlope';
DME : 'Dme';
VISUALMODEL : 'VisualModel';
BiasXYZ : 'BiasXYZ';
RUNWAYSTART : 'RunwayStart';
HELIPAD : 'Helipad';
NDB: 'Ndb';
START_TAG: 'Start';
TAXIWAYSIGN: 'TaxiwaySign';
VERTEX: 'Vertex';
GEOPOL: 'Geopol';
MARKER: 'Marker';
WAYPOINT: 'Waypoint';
APPROACH: 'Approach';
APPROACHLEGS : 'ApproachLegs';
LEG : 'Leg';
MISSEDAPPROACHLEAGS : 'MissedApproachLegs';
TRANSITION : 'Transition';
DMEARC : 'DmeArc';
TRANSITIONLEGS: 'TransitionLegs';

//ATTRIBUTES
LAT : 'lat';
LON : 'lon';
ALT : 'alt';
TYPE : 'type';
INDEX : 'index';
BIASX:'biasX';
BIASZ:'biasZ';
NAME : 'name';
NUMBER: 'number';
IDENT : 'ident';
MAGVAR : 'magvar';
AIRPORT_TEST_RADIUS : 'airportTestRadius';
TRAFFIC_SCALAR : 'trafficScalar';
REGION : 'region';
COUNTRY : 'country';
STATE : 'state';
CITY : 'city';
ORIENTATION: 'orientation';
AVAILABILITY : 'availability';
HEADING : 'heading';
RADIUS : 'radius';
AIRLINECODES : 'airlineCodes';
PUSHBACK : 'pushBack';
TEEOFFSET1 : 'teeOffset1';
TEEOFFSET2 : 'teeOffset2';
TEEOFFSET3 : 'teeOffset3';
TEEOFFSET4 : 'teeOffset4';
ROUTETYPE : 'routeType';
WAYPOINTTYPE : 'waypointType';
WAYPOINTREGION : 'waypointRegion';
WAYPOINTIDENT : 'waypointIdent';
ALTITUDEMINIMUN : 'altitudeMinimum';
START : 'start';
END : 'end';
WIDTH : 'width';
WEIGHT_LIMIT : 'weightLimit';
SURFACE : 'surface';
DRAW_SURFACE : 'drawSurface';
DRAW_DETAIL : 'drawDetail';
DESIGNATOR : 'designator';
CENTER_LINE : 'centerLine';
CENTER_LINE_LIGHTED : 'centerLineLighted';
LEFT_EDGE : 'leftEdge';
LEFT_EDGE_LIGHTED : 'leftEdgeLighted';
RIGHT_EDGE : 'rightEdge';
RIGHT_EDGE_LIGHTED : 'rightEdgeLighted';
FREQUENCY: 'frequency';
LENGTH: 'length';
PRIMARY_DESIGNATOR: 'primaryDesignator';
SECONDARY_DESIGNATOR: 'secondaryDesignator';
PATTERN_ALTITUDE: 'patternAltitude';
PRIMARY_TAKE_OFF: 'primaryTakeoff';
PRIMARY_LANDING: 'primaryLanding';
PRIMARY_PATTERN: 'primaryPattern';
SECONDARY_TAKE_OFF: 'secondaryTakeoff';
SECONDARY_LANDING: 'secondaryLanding';
SECONDARY_PATTERN: 'secondaryPattern';
PRIMARY_MARKING_BIAS: 'primaryMarkingBias';
SECONDARY_MARKING_BIAS: 'secondaryMarkingBias';
ALTERNATETHRESHOLD: 'alternateThreshold';
ALTERNATETOUCHDOWN: 'alternateTouchdown';
ALTERNATEFIXEDDISTANCE: 'alternateFixedDistance';
ALTERNATEPRECISION: 'alternatePrecision';
LEADINGZEROSIDENT: 'leadingZeroIdent';
NOTHRESHOLDENDARROWS: 'noThresholdEndArrows';
EDGES: 'edges';
THRESHOLD: 'threshold';
FIXED_DISTANCE: 'fixedDistance';
FIXED:'fixed';
TOUCHDOWN: 'touchdown';
DASHES: 'dashes';
PRECISION: 'precision';
EDGEPAVEMENT:'edgePavement';
SINGLEEND:'singleEnd';
PRIMARYCLOSE:'primaryClosed';
SECONDARYCLOSED:'secondaryClosed';
PRIMARYSTOL: 'primaryStol';
SECONDARYSTOL:'secondaryStol';
CENTER:'center';
EDGE:'edge';
CENTERRED:'centerRed';
SYSTEM: 'system';
REIL : 'reil';
ENDLIGHTS: 'endLights';
STROBES: 'strobes';
SIDE:'side';
SPACING:'spacing';
PITCH:'pitch';
RANGE: 'range';
BACKCOURSE: 'backCourse';
IMAGECOMPLEXITY: 'imageComplexity';
BIASY: 'biasY';
CLOSED: 'closed';
TRANSPARENT : 'transparent';
RED : 'red';
GREEN : 'green';
BLUE: 'blue';
LABEL: 'label';
SIZE: 'size';
JUSTIFICATION: 'justification';
FIXTYPE: 'fixType';
FIXREGION: 'fixRegion';
FIXIDENT: 'fixIdent';
ALTITUDE: 'altitude';
MISSALTITUDE: 'missedAltitude';
SUFFIX : 'suffix';
GPSOVERLAY : 'gpsOverlay';
FLYOVER: 'flyOver';
TURNDIRECTION: 'turnDirection';
RECOMMENDEDTYPE: 'recommendedType';
RECOMMENDEREGION: 'recommendedRegion';
RECOMMENDEIDENT: 'recommendedIdent';
THETA: 'theta';
RHO: 'rho';
TRUECOURSE: 'trueCourse';
MAGNETICCOURSE: 'magneticCourse';
DISTANCE: 'distance';
TIME: 'time';
ALTITUDEDESCRIPTOR: 'altitudeDescriptor';
ALTITUDE1: 'altitude1';
ALTITUDE2: 'altitude2';
TRANSITIONTYPE: 'transitionType';
RADIAL: 'radial';
DMELDENT: 'dmeIdent';
RUNWAY_ATTR : 'runway';

//SKIP TAGS
fragment SKIP_TAG: 'Jetway'
                   | 'SceneryObject'
                   | 'DeleteAirport'
                   | 'RunwayAlias'
                   | 'LibraryObject'
                   | 'Aprons'
                   | 'Apron'
                   | 'ApronEdgeLights'
                   | 'EdgeLights'
                   | 'TaxiwaySign'
                   | 'Vertex'
                   | 'Ndb'
                   | 'Start'
                   | 'BoundaryFence'
                   | 'BlastFence';

SKIP_TAG_ELEM: (('<' SKIP_TAG (~'>')* '>') 
               | ('</' SKIP_TAG '>')
               | ('<' SKIP_TAG (~'/')* '/>')) -> skip;

//INT
ATTR_VALUE_INT : (ATTR_DQ ('-')?(DIGIT)+ ATTR_DQ | ATTR_QU ('-')?(DIGIT)+ ATTR_QU);

//REAL
ATTR_VALUE_REAL : ((ATTR_DQ '-'? DIGIT+ '.' DIGIT+ ATTR_DQ) | (ATTR_QU '-'? DIGIT+ '.' DIGIT+ ATTR_QU));

//STR
ATTR_VALUE_STR : (ATTR_DQ (~'"')* ATTR_DQ | ATTR_QU (~'\'')* ATTR_QU);

NAMECHAR : LETTER | DIGIT | '.' | '-' | '_' | ':';
DIGIT : [0-9] ;
LETTER : [a-z] | [A-Z] ;
WS: [ \t\r\n]+ -> skip ;

//=====================================
//PARSER
//=====================================
//XML Doc
document : xmlElem EOF;

//XML Element
xmlElem : airportStart
          optionalElems*
          taxiwaypointElem+
          taxiwayparkingElem+
          taxiNameElem+
          taxiwayPathElem+
          optionalElems*
          airportEnd;

optionalElems : serviceElem
                | towerElem
                | runwayElem
                | comElem
                | approachElem
                | helipadElem
                | waypointElem;

//=====================================
//Airport
//=====================================
airportAttrReal : LAT
                | LON
                | MAGVAR
                | TRAFFIC_SCALAR
                | AIRPORT_TEST_RADIUS;

airportAttrInt : MAGVAR;

airportAttrStr : REGION
               | COUNTRY
               | STATE
               | CITY
               | NAME
               | ALT
               | IDENT
               | AIRPORT_TEST_RADIUS;

airportStart : TAG_START_OPEN AIRPORT
             ((airportAttrReal ATTR_EQ ATTR_VALUE_REAL) 
              | (airportAttrStr ATTR_EQ ATTR_VALUE_STR)
              | (airportAttrInt ATTR_EQ ATTR_VALUE_INT))*
             TAG_CLOSE; 

airportEnd : TAG_END_OPEN AIRPORT TAG_CLOSE;

//=====================================
//TaxiwayPoint
//=====================================
taxiwayPointAttStr: TYPE | ORIENTATION;

taxiwayPointAttReal: BIASX |
                     BIASZ |
                     LON |
                     LAT;

taxiwayPointAttInt: INDEX; 

taxiwaypointElem : TAG_START_OPEN TAXIWAYPOINT
                   (taxiwayPointAttInt ATTR_EQ ATTR_VALUE_INT |
                   taxiwayPointAttReal ATTR_EQ ATTR_VALUE_REAL |
                   taxiwayPointAttStr ATTR_EQ ATTR_VALUE_STR)* 
                   TAG_EMPTY_CLOSE;

//=====================================
//Services
//=====================================
serviceStart : TAG_START_OPEN SERVICES TAG_CLOSE;

serviceEnd : TAG_END_OPEN SERVICES TAG_CLOSE;

serviceElem : serviceStart fuelElem* serviceEnd;

//=====================================
//Fuel
//=====================================
fuelAttrStr : TYPE | AVAILABILITY;

fuelElem : TAG_START_OPEN FUEL 
         ((fuelAttrStr ATTR_EQ ATTR_VALUE_STR) | (TYPE ATTR_EQ ATTR_VALUE_INT))*
         TAG_EMPTY_CLOSE;

//=====================================
//Tower
//=====================================
towerAttrReal : LAT | LON;

towerAttrStr : ALT;

towerStart : TAG_START_OPEN TOWER
          ((towerAttrStr ATTR_EQ ATTR_VALUE_STR) | (towerAttrReal ATTR_EQ ATTR_VALUE_REAL))*;

towerEnd : TAG_EMPTY_CLOSE | (TAG_CLOSE TAG_END_OPEN TOWER TAG_CLOSE);

towerElem : towerStart towerEnd;

//=====================================
//TaxiwayParking
//=====================================
taxiwayparkingAttrInt: INDEX
                     | NUMBER;
                                 
taxiwayparkingAttrReal: LAT
                      | LON
                      | BIASX
                      | BIASZ
                      | HEADING
                      | RADIUS
                      | TEEOFFSET1
                      | TEEOFFSET2
                      | TEEOFFSET3
                      | TEEOFFSET4;
                          
taxiwayparkingAttrStr: TYPE
                     | NAME
                     | AIRLINECODES
                     | PUSHBACK
                     | RADIUS;

taxiwayparkingElem : TAG_START_OPEN TAXIWAYPARKING
                      ((taxiwayparkingAttrInt ATTR_EQ ATTR_VALUE_INT)
                       | (taxiwayparkingAttrReal ATTR_EQ ATTR_VALUE_REAL) 
                       | (taxiwayparkingAttrStr ATTR_EQ ATTR_VALUE_STR))*
                   TAG_EMPTY_CLOSE;

//=====================================
//ROUTE
//=====================================
routeAttrStr : ROUTETYPE
             | NAME;

routeStart:  TAG_START_OPEN ROUTE
             (routeAttrStr ATTR_EQ ATTR_VALUE_STR)*
             TAG_CLOSE; 

routeEnd : TAG_END_OPEN ROUTE TAG_CLOSE;

routeElem : routeStart
         (previouselement nextelement)?
          routeEnd;

//=====================================
//PREVIOUS
//=====================================
previousAttrStr : WAYPOINTTYPE
                | WAYPOINTREGION
                | WAYPOINTIDENT;
                   
previousAttrReal : ALTITUDEMINIMUN;

previouselement : TAG_START_OPEN PREVIOUS
                  ((previousAttrStr ATTR_EQ ATTR_VALUE_STR) | (previousAttrReal ATTR_EQ ( ATTR_VALUE_REAL | ATTR_VALUE_INT )))*
                  TAG_EMPTY_CLOSE;

//=====================================
//NEXT
//=====================================
nextAttrStr : WAYPOINTTYPE
               | WAYPOINTREGION
               | WAYPOINTIDENT;
                   
nextAttrReal : ALTITUDEMINIMUN;

nextelement : TAG_START_OPEN NEXT
                  ((nextAttrStr ATTR_EQ ATTR_VALUE_STR) | (nextAttrReal  ATTR_EQ (ATTR_VALUE_REAL | ATTR_VALUE_INT )))*
                  TAG_EMPTY_CLOSE;

//=====================================
//TaxiwayPath
//=====================================
taxiwayPathAttrInt: START | 
                    END |
                    NAME | 
                    WEIGHT_LIMIT |
                    NUMBER;

taxiwayPathAttrReal: WIDTH |
                     WEIGHT_LIMIT;

taxiwayPathAttrString: TYPE |
                       SURFACE |
                       DRAW_SURFACE |
                       DRAW_DETAIL |
                       CENTER_LINE |
                       CENTER_LINE_LIGHTED |
                       LEFT_EDGE |
                       LEFT_EDGE_LIGHTED |
                       RIGHT_EDGE |
                       RIGHT_EDGE_LIGHTED |
                       NUMBER |
                       DESIGNATOR |
                       WIDTH;

taxiwayPathElem : TAG_START_OPEN TAXIWAYPATH 
                  ((taxiwayPathAttrInt ATTR_EQ ATTR_VALUE_INT) |
                 (taxiwayPathAttrReal ATTR_EQ ATTR_VALUE_REAL) |
                 (taxiwayPathAttrString ATTR_EQ ATTR_VALUE_STR))*
                   TAG_EMPTY_CLOSE;

//=====================================
//RunwayAlias
//=====================================
/*
runwayAliasAttrString: DESIGNATOR |
                       NUMBER;

runwayAliasElem: TAG_START_OPEN RUNWAY_ALIAS
                 ((runwayAliasAttrString ATTR_EQ ATTR_VALUE_STR))*
                TAG_EMPTY_CLOSE;
*/
//=====================================
//Com
//=====================================
comElem: TAG_START_OPEN COM 
         (FREQUENCY ATTR_EQ ATTR_VALUE_REAL |
         TYPE ATTR_EQ ATTR_VALUE_STR |
         NAME ATTR_EQ ATTR_VALUE_STR)+
         TAG_EMPTY_CLOSE;

//=====================================
//TaxiName
//=====================================
taxiNameElem: TAG_START_OPEN TAXINAME 
              (INDEX ATTR_EQ ATTR_VALUE_INT | NAME ATTR_EQ ATTR_VALUE_STR)+
              TAG_EMPTY_CLOSE;

//=====================================
//Runway
//=====================================
runwayAttString: 
                 ALT |
                 SURFACE |
                 LENGTH |
                 WIDTH |
                 NUMBER |
                 DESIGNATOR |
                 PRIMARY_DESIGNATOR |
                 SECONDARY_DESIGNATOR |
                 PATTERN_ALTITUDE |                
                 PRIMARY_TAKE_OFF |
                 PRIMARY_LANDING |
                 PRIMARY_PATTERN |
                 SECONDARY_TAKE_OFF |
                 SECONDARY_LANDING |
                 SECONDARY_PATTERN |
                 PRIMARY_MARKING_BIAS |
                 SECONDARY_MARKING_BIAS;

runwayAttReal: LAT |
               LON |
               HEADING |
               ALT |
               NUMBER |
               DESIGNATOR |
               PRIMARY_DESIGNATOR |
               SECONDARY_DESIGNATOR |
               PATTERN_ALTITUDE;

runwayAttInt: NUMBER;

runwayNestedElem: markingsElem
                | lightsElem
                | offsetThresholdElem
                | blastPadElem
                | overrunElem
                | approachLightsElem
                | vasiElem
                | ilsElem
                | runwayStartElem;

runwayElemStart: TAG_START_OPEN RUNWAY 
            (runwayAttString ATTR_EQ ATTR_VALUE_STR|
            runwayAttReal ATTR_EQ ATTR_VALUE_REAL |
            runwayAttInt ATTR_EQ ATTR_VALUE_INT)*
            TAG_CLOSE;

runwayElemClose: TAG_END_OPEN RUNWAY TAG_CLOSE;

runwayElem: runwayElemStart runwayNestedElem* runwayElemClose;


//=====================================
//Markings
//=====================================
markingsAttsBool: 
                  ALTERNATETHRESHOLD |
                  ALTERNATETOUCHDOWN |
                  ALTERNATEFIXEDDISTANCE |
                  ALTERNATEPRECISION |
                  LEADINGZEROSIDENT |
                  NOTHRESHOLDENDARROWS |
                  EDGES |
                  THRESHOLD |
                  FIXED |
                  TOUCHDOWN |
                  DASHES |
                  IDENT |
                  PRECISION |
                  EDGEPAVEMENT |
                  SINGLEEND |
                  PRIMARYCLOSE |
                  SECONDARYCLOSED |
                  PRIMARYSTOL |
                  SECONDARYSTOL |
                  FIXED_DISTANCE;

markingsElem : TAG_START_OPEN MARKINGS
               (markingsAttsBool ATTR_EQ ATTR_VALUE_STR)+
               TAG_EMPTY_CLOSE;

//=====================================
//Lights
//=====================================
lightsAttStr: CENTER 
            | EDGE
            | CENTERRED;

lightsElem : TAG_START_OPEN LIGHTS
               (lightsAttStr ATTR_EQ ATTR_VALUE_STR)+
               TAG_EMPTY_CLOSE;

//=====================================
//OffsetThreshold
//=====================================
offsetThresholdAttStr: END |
                     WIDTH |
                     LENGTH |
                     SURFACE
;
offsetThresholdElem : TAG_START_OPEN OFFSETTHRESHOLD
                      (offsetThresholdAttStr ATTR_EQ ATTR_VALUE_STR)*
                      TAG_EMPTY_CLOSE
;

//=====================================
//BlastPad
//=====================================
blastPadAttStr: END |
               LENGTH |
               WIDTH |
               SURFACE
              ;
blastPadElem: TAG_START_OPEN BLASTPAD
              (blastPadAttStr ATTR_EQ ATTR_VALUE_STR)+
              TAG_EMPTY_CLOSE;

//=====================================
//Overrun
//=====================================
overrunAttStr: END |
               LENGTH |
               WIDTH |
               SURFACE
              ;
overrunElem: TAG_START_OPEN OVERRUN
              (overrunAttStr ATTR_EQ ATTR_VALUE_STR)+
              TAG_EMPTY_CLOSE;


//=====================================
//ApproachLights
//=====================================
approachLightsAttStr: END 
                    | SYSTEM
                    | REIL 
                    | TOUCHDOWN 
                    | ENDLIGHTS
                    ;

approachLightsAttPosInt: STROBES;

approachLightsElem: TAG_START_OPEN APPROACHLIGHTS
                    (approachLightsAttPosInt ATTR_EQ ATTR_VALUE_INT |
                    approachLightsAttStr ATTR_EQ ATTR_VALUE_STR)+
                    TAG_EMPTY_CLOSE;

//=====================================
//Vasi
//=====================================
vasiAttStr: END |
          TYPE |
          SIDE |
          BIASX |
          BIASZ |
          SPACING;

vasiElem: TAG_START_OPEN VASI
          ( vasiAttStr ATTR_EQ ATTR_VALUE_STR
          | PITCH ATTR_EQ (ATTR_VALUE_REAL | ATTR_VALUE_INT)
          )+
          TAG_EMPTY_CLOSE;


//=====================================
//ILS
//=====================================
ilsAttStr: NAME
         | END
         | IDENT
         | BACKCOURSE
         | ALT
         | RANGE;

ilsAttReal: LAT |
           LON |
           HEADING |
           FREQUENCY |
           MAGVAR |
           WIDTH
          ;

ilsAttInt : MAGVAR;

ilsElemStart:
        TAG_START_OPEN ILS
        ( ilsAttInt ATTR_EQ ATTR_VALUE_INT
        | ilsAttStr ATTR_EQ ATTR_VALUE_STR
        | ilsAttReal ATTR_EQ ATTR_VALUE_REAL
        )+
        TAG_CLOSE
       ;

ilsElemClose:
             TAG_END_OPEN ILS TAG_CLOSE;

ilsElem: ilsElemStart
         glideSlopeElem?
         dmeElem?
         visualModelElem?
         ilsElemClose;

//=====================================
//GlideSlope
//=====================================
glideSlopeAttReal: LAT
                 | LON
                 | ALT
                 | PITCH
                 ;
glideSlopeAttInt: RANGE;

glideSlopeAttStr: LAT
                 | LON
                 | RANGE
                 | ALT
                ;

glideSlopeElem:   TAG_START_OPEN GLIDESLOPE
                  ( glideSlopeAttReal ATTR_EQ ATTR_VALUE_REAL
                  | glideSlopeAttInt ATTR_EQ ATTR_VALUE_INT 
                  | glideSlopeAttStr ATTR_EQ ATTR_VALUE_STR
                  )+
                  TAG_EMPTY_CLOSE;

//=====================================
//DME
//=====================================
dmeAttReal: LAT
                 | LON
                 | ALT
                 ;
dmeAttStr: LAT
                 | LON
                 | ALT
                 | RANGE
                 ;

dmeElem:   TAG_START_OPEN DME
                  ( dmeAttReal ATTR_EQ ATTR_VALUE_REAL
                  | RANGE ATTR_EQ ATTR_VALUE_INT 
                  | dmeAttStr ATTR_EQ ATTR_VALUE_STR)+
                  TAG_EMPTY_CLOSE;


//=====================================
//VisualModel
//=====================================
visualModelAttReal: HEADING
                  ;
visualModelAttStr: IMAGECOMPLEXITY
                 | NAME;
visualModelElemStart: TAG_START_OPEN VISUALMODEL
                  ( visualModelAttReal ATTR_EQ ATTR_VALUE_REAL
                  | visualModelAttStr ATTR_EQ ATTR_VALUE_STR ) +
                  TAG_CLOSE;

visualModelElemClose:
             TAG_END_OPEN VISUALMODEL TAG_CLOSE;

visualModelElem: visualModelElemStart biasXYZElem? visualModelElemClose;

//=====================================
//BiasXYZ
//=====================================
biasXYZAttReal: BIASX
              | BIASY
              | BIASZ
;
biasXYZAttInt: BIASX
             | BIASY
             | BIASZ
;
                

biasXYZElem: TAG_START_OPEN BiasXYZ
                  ( biasXYZAttReal ATTR_EQ ATTR_VALUE_REAL
                  | biasXYZAttInt ATTR_EQ ATTR_VALUE_INT)+
                  TAG_EMPTY_CLOSE;

//=====================================
//RunwayStart
//=====================================
runwayStartAttReal: LAT
                  | LON
                  | ALT
                  | HEADING
                  ;
runwayStartAttStr: TYPE
                 | END
                 ;

runwayStartElem: TAG_START_OPEN RUNWAYSTART
                 ( runwayStartAttReal ATTR_EQ ATTR_VALUE_REAL
                 | runwayStartAttStr ATTR_EQ ATTR_VALUE_STR)+
                 TAG_EMPTY_CLOSE;

//=====================================
//Helipad
//=====================================
helipadAttStr: LAT
             | LON
             | ALT
             | SURFACE
             | LENGTH
             | WIDTH
             | TYPE
             | CLOSED
             | TRANSPARENT;

helipadAttReal: HEADING
              | LENGTH
              | WIDTH;

helipadAttInt: RED
             | GREEN
             | BLUE
             | HEADING;


helipadElem: TAG_START_OPEN HELIPAD
             ( helipadAttInt ATTR_EQ ATTR_VALUE_INT
             | helipadAttReal ATTR_EQ ATTR_VALUE_REAL
             | helipadAttStr ATTR_EQ ATTR_VALUE_STR ) *
             TAG_EMPTY_CLOSE;

//=====================================
//Ndb
//=====================================
/*
ndbAttStr: LAT
         | LON
         | TYPE
         | RANGE
         | REGION
         | IDENT
         | NAME;

ndbAttReal: LAT
         | LON
         | ALT
         | FREQUENCY
         | MAGVAR;

ndbAttInt: LAT
         | LON
         | ALT
         | RANGE;

ndbElemStart: TAG_START_OPEN NDB
                  ( ndbAttReal ATTR_EQ ATTR_VALUE_REAL
                  | ndbAttStr ATTR_EQ ATTR_VALUE_STR
                  | ndbAttInt ATTR_EQ ATTR_VALUE_INT)+
                  TAG_CLOSE;

ndbElemClose: TAG_END_OPEN NDB TAG_CLOSE;

ndbElem: ndbElemStart visualModelElem* ndbElemClose ;
*/
//=====================================
//Start
//=====================================
/*
startAttrInt: NUMBER;

startAttrReal: LAT 
             | LON
             | HEADING;

startAttrString: TYPE
               | NUMBER 
               | DESIGNATOR
               | ALT;
           
startElem: TAG_START_OPEN START_TAG
           ((startAttrInt ATTR_EQ ATTR_VALUE_INT) |
           (startAttrReal ATTR_EQ ATTR_VALUE_REAL) |
           (startAttrString ATTR_EQ ATTR_VALUE_STR))*
           TAG_EMPTY_CLOSE;
*/
//=====================================
//TaxiwaySign
//=====================================
/*
taxiwaySignAttrReal: LAT
                   | LON
                   | HEADING;


taxiwaySignAttrString: LABEL
                     | JUSTIFICATION 
                     | SIZE;
                 
taxiwaySignElem: TAG_START_OPEN TAXIWAYSIGN
                 ((taxiwaySignAttrReal ATTR_EQ ATTR_VALUE_REAL) |
                 (taxiwaySignAttrString ATTR_EQ ATTR_VALUE_STR))*
                 TAG_EMPTY_CLOSE;
*/
//=====================================
//Vertex
//=====================================
vertexAttrReal: BIASX 
              | BIASZ
              | LAT
              | LON;

vertexElem: TAG_START_OPEN VERTEX
            ((vertexAttrReal ATTR_EQ ATTR_VALUE_REAL))*
            TAG_EMPTY_CLOSE;

//=====================================
//Geopol
//=====================================
geopolAttrString: TYPE;

geopolElemStart: TAG_START_OPEN GEOPOL
                 geopolAttrString ATTR_EQ ATTR_VALUE_STR
                 TAG_CLOSE;

geopolElemClose: TAG_END_OPEN GEOPOL TAG_CLOSE;

geopolElem: geopolElemStart vertexElem* geopolElemClose;


//=====================================
//Marker
//=====================================
markerAttrReal: LAT 
              | LON 
              | ALT 
              | HEADING;

markerAttrString: TYPE 
                | REGION
                | IDENT;

markerElem: TAG_START_OPEN MARKER 
            ((markerAttrReal ATTR_EQ ATTR_VALUE_REAL) |
            (markerAttrString ATTR_EQ ATTR_VALUE_STR))*
            TAG_EMPTY_CLOSE;

//=====================================
//Waypoint
//=====================================
waypointAttrReal: LAT 
                | LON
                | MAGVAR;

waypointAttrInt: MAGVAR;

waypointAttrString: WAYPOINTTYPE
                  | WAYPOINTREGION
                  | WAYPOINTIDENT;

waypointElemStart: TAG_START_OPEN WAYPOINT 
                   ((waypointAttrReal ATTR_EQ ATTR_VALUE_REAL) 
                   |(waypointAttrString ATTR_EQ ATTR_VALUE_STR)
                   |(waypointAttrInt ATTR_EQ ATTR_VALUE_INT))*
                   TAG_CLOSE;

waypointElemClose: TAG_END_OPEN WAYPOINT TAG_CLOSE;

waypointElem: waypointElemStart routeElem* waypointElemClose;

//=====================================
//APPROACH
//=====================================

approachAttrStr : TYPE
                | FIXTYPE
                | FIXREGION
                | FIXIDENT
                | RUNWAY_ATTR
                | DESIGNATOR
                | SUFFIX
                | GPSOVERLAY
                | MISSALTITUDE
                | ALTITUDE;

approachAttrReal : ALTITUDE 
                 | HEADING
                 | MISSALTITUDE;
 
approachAttrInt : RUNWAY_ATTR
                | SUFFIX;

approachStart:  TAG_START_OPEN APPROACH
            ((approachAttrStr ATTR_EQ ATTR_VALUE_STR)  
             | (approachAttrInt ATTR_EQ ATTR_VALUE_INT) 
             | (approachAttrReal ATTR_EQ ( ATTR_VALUE_REAL | ATTR_VALUE_INT )))*
             TAG_CLOSE; 

approachEnd : TAG_END_OPEN APPROACH TAG_CLOSE;

approachElem : approachStart approachlegsElem* missedapproachlegsElem* transitionElem* approachEnd;
    
//=====================================
//approachlegs
//=====================================

approachlegsElem : TAG_START_OPEN APPROACHLEGS TAG_CLOSE
                  (legelement)*
                  TAG_END_OPEN APPROACHLEGS TAG_CLOSE;

//=====================================
//Leg
//=====================================
    
legAttrStr :    TYPE
                | FIXTYPE
                | FIXREGION
                | FIXIDENT
                | FLYOVER
                | TURNDIRECTION
                | RECOMMENDEDTYPE
                | RECOMMENDEREGION
                | RECOMMENDEIDENT
                | RHO
                | DISTANCE
                | ALTITUDE1
                | ALTITUDE2
                | ALTITUDEDESCRIPTOR;

legAttrReal :   THETA
                | RHO
                | TRUECOURSE
                | MAGNETICCOURSE 
                | DISTANCE 
                | TIME 
                | ALTITUDE1 
                | ALTITUDE2;

legelement : TAG_START_OPEN LEG
             ((legAttrStr ATTR_EQ ATTR_VALUE_STR)  | (legAttrReal ATTR_EQ ( ATTR_VALUE_REAL | ATTR_VALUE_INT )))*
                   TAG_EMPTY_CLOSE;

//=====================================
//MissedApproachLegs
//=====================================

missedapproachlegsElem : TAG_START_OPEN MISSEDAPPROACHLEAGS TAG_CLOSE
                  (legelement)*
                  TAG_END_OPEN MISSEDAPPROACHLEAGS TAG_CLOSE;

//=====================================
//Transition
//=====================================

transitionAttrStr : TRANSITIONTYPE
                    | FIXTYPE
                    | FIXREGION
                    | FIXIDENT
                    | ALTITUDE;

transitionAttrReal : ALTITUDE;
 
transitionAttrInt :  ;

 transitionElem :transitionStart
            dmearc*
        transitionlegs+
          transitionEnd;

transitionStart:  TAG_START_OPEN TRANSITION
            ( (transitionAttrStr ATTR_EQ ATTR_VALUE_STR)  | (transitionAttrInt ATTR_EQ ATTR_VALUE_INT) | (transitionAttrReal ATTR_EQ ( ATTR_VALUE_REAL | ATTR_VALUE_INT )))*
             TAG_CLOSE; 

transitionEnd : TAG_END_OPEN TRANSITION TAG_CLOSE;

//=====================================
//DmeArc
//=====================================

dmearcAttrStr : DISTANCE
                | REGION
                | DMELDENT;
          
dmearcAttrReal : ;
           
dmearcAttrInt : RADIAL 
                | DISTANCE
                | REGION;

dmearc : TAG_START_OPEN DMEARC 
         ((dmearcAttrStr ATTR_EQ ATTR_VALUE_STR)  
          | (dmearcAttrInt ATTR_EQ ATTR_VALUE_INT) 
          | (dmearcAttrReal ATTR_EQ ( ATTR_VALUE_REAL | ATTR_VALUE_INT )))*
         TAG_EMPTY_CLOSE;

//=====================================
//TRANSITIONLEGS
//=====================================

transitionlegs:    TAG_START_OPEN TRANSITIONLEGS TAG_CLOSE
                  ( legelement)*
                  TAG_END_OPEN TRANSITIONLEGS TAG_CLOSE;
